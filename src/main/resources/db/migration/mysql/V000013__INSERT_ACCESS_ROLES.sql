INSERT INTO ROLE (DS_NAME) VALUES ('ROLE_PERSON_READ');
INSERT INTO ROLE (DS_NAME) VALUES ('ROLE_PERSON_WRITE');

INSERT INTO ROLE (DS_NAME) VALUES ('ROLE_PROFILE_READ');
INSERT INTO ROLE (DS_NAME) VALUES ('ROLE_PROFILE_WRITE');

INSERT INTO ROLES_PROFILE (ID_PROFILE, ID_ROLE) VALUE (1, 6);
INSERT INTO ROLES_PROFILE (ID_PROFILE, ID_ROLE) VALUE (1, 7);
INSERT INTO ROLES_PROFILE (ID_PROFILE, ID_ROLE) VALUE (1, 8);
INSERT INTO ROLES_PROFILE (ID_PROFILE, ID_ROLE) VALUE (1, 9);