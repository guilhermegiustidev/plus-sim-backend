-- -----------------------------------------------------
-- Table `MODULE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MODULE` (
  `ID_MODULE` INT NOT NULL AUTO_INCREMENT,
  `CD_MODULE` VARCHAR(45) NOT NULL,
  `STATUS` TINYINT NULL,
  PRIMARY KEY (`ID_MODULE`))
ENGINE = InnoDB;

