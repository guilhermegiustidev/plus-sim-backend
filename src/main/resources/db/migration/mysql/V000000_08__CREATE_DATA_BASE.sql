-- -----------------------------------------------------
-- Table `PRODUCT`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PRODUCT` (
  `ID_PRODUCT` INT NOT NULL AUTO_INCREMENT,
  `DS_NAME` VARCHAR(200) NOT NULL,
  `STATUS` TINYINT NOT NULL,
  PRIMARY KEY (`ID_PRODUCT`))
ENGINE = InnoDB;
