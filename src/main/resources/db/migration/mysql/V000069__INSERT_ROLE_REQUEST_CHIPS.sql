INSERT INTO ROLE (DS_NAME) VALUE ('ROLE_REQUESTSIMCHIP_READ');
INSERT INTO ROLE (DS_NAME) VALUE ('ROLE_REQUESTSIMCHIP_WRITE');

TRUNCATE TABLE ROLES_PROFILE;

INSERT INTO ROLES_PROFILE (ID_PROFILE, ID_ROLE)
SELECT 1, ID_ROLE FROM ROLE;