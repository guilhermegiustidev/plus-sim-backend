-- -----------------------------------------------------
-- Table `SIN_LINE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIN_LINE` (
  `ID_SIN` INT NOT NULL AUTO_INCREMENT,
  `DS_NUMBER` VARCHAR(100) NOT NULL,
  `TP_STATUS` VARCHAR(45) NOT NULL,
  `DT_RELEASED` TIMESTAMP(6) NOT NULL,
  PRIMARY KEY (`ID_SIN`),
  UNIQUE INDEX `DS_NUMBER_UNIQUE` (`DS_NUMBER` ASC))
ENGINE = InnoDB;
