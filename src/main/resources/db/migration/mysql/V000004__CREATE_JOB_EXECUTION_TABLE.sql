CREATE TABLE `JOB_PROCESS` (
  `ID_JOB` INT NOT NULL AUTO_INCREMENT,
  `DS_NAME` VARCHAR(100) NULL,
  `IDENTIFIER` VARCHAR(300) NULL,
  `LAST_EXECUTION` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `NM_TRY` INT NULL DEFAULT 0,
  `STATUS` VARCHAR(50) NULL,
  PRIMARY KEY (`ID_JOB`));
