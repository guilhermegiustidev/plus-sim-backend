ALTER TABLE `SIN_PLAN` 
ADD COLUMN `VL_PRICE` DOUBLE NOT NULL AFTER `DS_NAME`,
ADD COLUMN `HR_DURATION` INT NOT NULL AFTER `VL_PRICE`,
ADD COLUMN `VL_COST` DOUBLE NOT NULL AFTER `HR_DURATION`,
ADD COLUMN `VL_PROFIT` DOUBLE NOT NULL AFTER `VL_COST`;
