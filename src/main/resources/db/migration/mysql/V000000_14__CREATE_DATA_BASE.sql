	
-- -----------------------------------------------------
-- Table `SIN_PLAN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIN_PLAN` (
  `ID_PLAN` INT NOT NULL AUTO_INCREMENT,
  `DS_NAME` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`ID_PLAN`))
ENGINE = InnoDB;
