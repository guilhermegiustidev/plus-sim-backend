
-- -----------------------------------------------------
-- Table `PERFORMING`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PERFORMING` (
  `ID_PERFORMING` INT NOT NULL AUTO_INCREMENT,
  `DS_NAME` VARCHAR(100) NOT NULL,
  `DS_PHONE` VARCHAR(45) NOT NULL,
  `STATUS` TINYINT NOT NULL,
  `DS_EMAIL` VARCHAR(100) NULL,
  PRIMARY KEY (`ID_PERFORMING`))
ENGINE = InnoDB;

