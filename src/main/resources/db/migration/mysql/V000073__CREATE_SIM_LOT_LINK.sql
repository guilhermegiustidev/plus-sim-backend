CREATE TABLE `PLUS_CHIP_SIM_LOT` (
  `ID_PLUS_SIM_LOT_CHIP` INT NOT NULL AUTO_INCREMENT,
  `ID_SIM` INT NOT NULL,
  `ID_LOT` INT NOT NULL,
  `DT_TIME_INCLUDE` TIMESTAMP(6) NULL,
  `DT_TIME_LEAVE` TIMESTAMP(6) NULL,
  PRIMARY KEY (`ID_PLUS_SIM_LOT_CHIP`),
  INDEX `fk_PLUS_CHIP_SIM_LOT_1_idx` (`ID_SIM` ASC),
  INDEX `fk_PLUS_CHIP_SIM_LOT_2_idx` (`ID_LOT` ASC),
  CONSTRAINT `fk_PLUS_CHIP_SIM_LOT_1`
    FOREIGN KEY (`ID_SIM`)
    REFERENCES `SIM_CHIP` (`ID_SIN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PLUS_CHIP_SIM_LOT_2`
    FOREIGN KEY (`ID_LOT`)
    REFERENCES `SIN_LOT` (`ID_LOT`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
