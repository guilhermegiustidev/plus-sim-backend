
-- -----------------------------------------------------
-- Table `BRANCH`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BRANCH` (
  `ID_BRANCH` INT NOT NULL AUTO_INCREMENT,
  `DS_BRANCH` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`ID_BRANCH`))
ENGINE = InnoDB;
