-- -----------------------------------------------------
-- Table `PROFILE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PROFILE` (
  `ID_PROFILE` INT NOT NULL AUTO_INCREMENT,
  `DS_NAME` VARCHAR(200) NOT NULL,
  `STATUS` TINYINT NOT NULL,
  PRIMARY KEY (`ID_PROFILE`))
ENGINE = InnoDB;
