package br.com.gst.soft.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProgramValidationResponse {

	private boolean canNavigate;

	private String labels;
	
}
