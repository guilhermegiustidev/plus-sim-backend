package br.com.gst.soft.response;

import br.com.gst.soft.entity.sim.Sim.SimChipStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SimInvoiceResponse {

	private Long simId;
	
	private Long lotId;
	
	private String code;
	
	private Long providerId;
	
	private String title;
	private String message;
	
	private boolean available;

	private SimChipStatus status;
}
