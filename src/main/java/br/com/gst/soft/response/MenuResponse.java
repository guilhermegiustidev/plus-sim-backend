package br.com.gst.soft.response;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MenuResponse {

	private String data;
	private String label;
	private List<MenuResponse> children;
	
}
