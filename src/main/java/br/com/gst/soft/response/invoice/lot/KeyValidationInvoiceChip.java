package br.com.gst.soft.response.invoice.lot;

import br.com.gst.soft.entity.provider.Provider;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class KeyValidationInvoiceChip {
	private ValidationInvoiceLotItemType type;
	private Provider provider;
}
