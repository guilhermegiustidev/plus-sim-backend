package br.com.gst.soft.response.invoice.sim;

public enum ValidationInvoiceChipItemStatus {

	AVAILABLE,
	OUT_OF_STOCK;
	
}
