package br.com.gst.soft.response.invoice.sim;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ValidationInvoiceChipItemResponse {

	private Long providerId;
	private Integer quantity;
	private BigDecimal valorDeVenda;
	private ValidationInvoiceChipItemStatus status;
	
}
