package br.com.gst.soft.response;

import java.io.Serializable;

import br.com.gst.soft.entity.config.ConfigLayout;
import lombok.Data;

@Data
public class PersonDTO implements Serializable{

	private static final long serialVersionUID = -962015341400237038L;

	private Long id;
	
	private String name;
	
	private String email;
	
	private String login;
	
	private boolean status;
	
	private ConfigLayout config;
	
	private String avatar;
	
}
