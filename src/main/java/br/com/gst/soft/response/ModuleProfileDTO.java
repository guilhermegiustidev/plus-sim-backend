package br.com.gst.soft.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModuleProfileDTO implements Serializable{

	private static final long serialVersionUID = 8584622164689806757L;
	private String data;
	private Long profileId;
}
