package br.com.gst.soft.response;

import lombok.Data;

@Data
public class BranchDTO {

	private Long id;
	private String name;
	private boolean status;
	
}
