package br.com.gst.soft.response;

import java.io.Serializable;

import br.com.gst.soft.exception.PojoValidationException;
import br.com.gst.soft.rules.IPojoParameter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileDTO implements IPojoParameter, Serializable{

	private static final long serialVersionUID = 8390437092626018260L;

	private Long idUser;
	private Long idProfile;
	private boolean status;
	private String userName;
	
	@Override
	public void validate() throws PojoValidationException {}
	
	
}
