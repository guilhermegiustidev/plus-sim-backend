package br.com.gst.soft.response.invoice.sim;

public enum ValidationInvoiceChipStatus {

	INCOMPLETE,
	COMPLETE;
	
}
