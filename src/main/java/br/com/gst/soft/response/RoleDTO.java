package br.com.gst.soft.response;

import java.io.Serializable;

import br.com.gst.soft.exception.PojoValidationException;
import br.com.gst.soft.rules.IPojoParameter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleDTO implements IPojoParameter, Serializable{

	private static final long serialVersionUID = 8390437092626018260L;

	private Long id;
	private Long idProfile;
	private boolean status;
	private String roleName;
	
	@Override
	public void validate() throws PojoValidationException {}
	
	
}
