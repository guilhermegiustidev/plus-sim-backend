package br.com.gst.soft.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SimResponse {

	private Long idSim;
	
	private String code;
	
	private boolean valid;
	
	private Long lotId;
	
	private String lotCode;
	
	private Long providerId;
	
}
