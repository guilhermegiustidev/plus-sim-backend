package br.com.gst.soft.response.invoice.lot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Index;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gst.soft.entity.request.sim.RequestItemSimChip;
import br.com.gst.soft.response.SimResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties({ "sims" })
public class ValidationInvoiceLotResponse {

	private ValidationInvoiceLotType status;

	private Set<SimResponse> sims;

	private Map<KeyValidationInvoiceChip, Set<SimResponse>> map;

	public boolean addSim(SimResponse simResponse) {
		if (this.sims == null) {
			this.sims = new HashSet<SimResponse>();
		}
		if (simResponse != null) {
			return this.sims.add(simResponse);
		}
		return false;
	}

	public void processItem(RequestItemSimChip item) {
		if (CollectionUtils.isNotEmpty(this.sims) && item.getProvider() != null) {
			Set<SimResponse> sets = this.sims.stream()
					.filter(index -> index.getProviderId().equals(item.getProvider().getId()))
					.collect(Collectors.toSet());
			ValidationInvoiceLotItemType type = null;

			if (item.getQuantity() > sets.size()) {
				type = ValidationInvoiceLotItemType.INCOMPLETE;
			} else if (item.getQuantity() < sets.size()) {
				type = ValidationInvoiceLotItemType.EXCEEDED;
			} else {
				type = ValidationInvoiceLotItemType.COMPLETE;
			}

			if (map == null) {
				map = new HashMap<KeyValidationInvoiceChip, Set<SimResponse>>();
			}

			KeyValidationInvoiceChip key = KeyValidationInvoiceChip.builder().provider(item.getProvider()).type(type)
					.build();
			map.put(key, sets);
		}

	}

	public ValidationInvoiceLotResponse finalizeLot() {
		this.status = ValidationInvoiceLotType.EMPTY;
		if (this.map != null) {
			if (analyseItemsByStatus(ValidationInvoiceLotItemType.INCOMPLETE)) {
				doesntMatch();
				return this;
			}
			if (analyseItemsByStatus(ValidationInvoiceLotItemType.COMPLETE)) {
				complete();
				return this;
			}
			if (analyseItemsByStatus(ValidationInvoiceLotItemType.EXCEEDED)) {
				doesntMatch();
				return this;
			}
		}
		return this;
	}

	public ValidationInvoiceLotType complete() {
		return this.status = ValidationInvoiceLotType.COMPLETE;
	}

	public ValidationInvoiceLotType doesntMatch() {
		return this.status = ValidationInvoiceLotType.NO_MATCH;
	}

	public void notInFilial() {
		this.status = ValidationInvoiceLotType.NOT_IN_FILIAL;
	}

	private boolean analyseItemsByStatus(ValidationInvoiceLotItemType statusItem) {
		for (KeyValidationInvoiceChip key : map.keySet()) {
			if (key.getType().equals(statusItem)) {
				return true;
			}
		}
		return false;
	}

}
