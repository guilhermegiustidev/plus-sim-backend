package br.com.gst.soft.response;

import java.util.List;
import java.util.Set;

import br.com.gst.soft.exception.PojoValidationException;
import br.com.gst.soft.rules.IPojoParameter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfilePojo implements IPojoParameter{

	private Long idProfile;
	private String nameProfile;
	private boolean status;
	
	private List<UserProfileDTO> users;
	private List<RoleDTO>  roles;
	private Set<ModuleProfileDTO> modules;
	
	@Override
	public void validate() throws PojoValidationException {
		
	}
	
	
}
