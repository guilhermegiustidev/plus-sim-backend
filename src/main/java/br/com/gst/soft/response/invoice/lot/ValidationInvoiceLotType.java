package br.com.gst.soft.response.invoice.lot;

public enum ValidationInvoiceLotType {

	COMPLETE,
	INCOMPLETE,
	NOT_IN_FILIAL,
	NO_MATCH,
	EMPTY;
	
}
