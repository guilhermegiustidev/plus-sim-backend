package br.com.gst.soft.response.invoice.sim;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.NumberUtils;

import lombok.Data;

@Data
public class ValidationInvoiceChipResponse {

	private BigDecimal invoiceValue;
	private List<ValidationInvoiceChipItemResponse> items;
	private ValidationInvoiceChipStatus status;
	
	
	public ValidationInvoiceChipResponse() {
		this.invoiceValue = BigDecimal.ZERO;
		this.items = new ArrayList<ValidationInvoiceChipItemResponse>();
	}

	public void addItem(Long providerId, Integer quantity, Long stockQuantity, BigDecimal valorDeVenda) {
		ValidationInvoiceChipItemStatus status = ValidationInvoiceChipItemStatus.AVAILABLE;
		
		if(quantity != null && stockQuantity != null && stockQuantity <= quantity) {
			status = ValidationInvoiceChipItemStatus.OUT_OF_STOCK;
		}
		items.add(ValidationInvoiceChipItemResponse.builder().quantity(quantity).valorDeVenda(valorDeVenda).providerId(providerId).status(status)
				.build());
	}

	public ValidationInvoiceChipResponse procced() {
		this.status = ValidationInvoiceChipStatus.COMPLETE;
		this.items.forEach(index -> {
			if(index.getQuantity() != null && index.getValorDeVenda() != null) {
				this.invoiceValue=invoiceValue.add(BigDecimal.valueOf(index.getValorDeVenda().doubleValue() * index.getQuantity()));
			}
			if(ValidationInvoiceChipItemStatus.OUT_OF_STOCK.equals(index.getStatus())) {
				this.status = ValidationInvoiceChipStatus.INCOMPLETE;
			}
		});
		return this;
	}

}
