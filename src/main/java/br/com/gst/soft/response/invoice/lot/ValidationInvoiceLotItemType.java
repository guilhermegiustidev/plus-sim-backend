package br.com.gst.soft.response.invoice.lot;

public enum ValidationInvoiceLotItemType {
	COMPLETE,
	INCOMPLETE,
	EXCEEDED,
}

