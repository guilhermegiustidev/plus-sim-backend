package br.com.gst.soft.response;

import lombok.Data;

@Data
public class PartnerDTO {

	private Long id;
	private String name;
	private boolean status;
}
