package br.com.gst.soft.auth;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.UserOperations;
import org.springframework.stereotype.Component;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.secutiry.Profile;
import br.com.gst.soft.repository.ProfileRepository;
import br.com.gst.soft.repository.UserRepository;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserBuildApplication {

	@Value("${spring.simple-profile}")
	private String simpleProfile;
	
	@Autowired
	public UserRepository userRepository;

	@Autowired
	public ProfileRepository profileRepository;

	public Set<Profile> getDefaultProfile() {
		 Profile profile = profileRepository.findByDescription(simpleProfile);
		 Set<Profile> profiles = new HashSet<Profile>();
		 profiles.add(profile);
		 return profiles;
	}

	public boolean createUser(User facebookUser, Person applicationUser, UserOperations userOperations, String userName) {
		return true;
	}
}
