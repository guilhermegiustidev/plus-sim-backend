package br.com.gst.soft.resource;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.menu.BadgeMenu;
import br.com.gst.soft.menu.MenuProcessor;
import br.com.gst.soft.response.MenuResponse;
import br.com.gst.soft.service.ProgramService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/menu")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MenuResource implements Serializable{

	private static final long serialVersionUID = -4006665563851471344L;

	@Autowired
	private ApplicationContext application;

	@Autowired
	private ProgramService programService;
	
	@GetMapping(value = "/permission")
	public boolean permission(@RequestParam(value = "menuId", required = true, name = "menuId") final String menuId) {
		return programService.verifyPermission(menuId);
	}
	
	@GetMapping(value = "/findMenus")
	public List<MenuResponse> findMenus(){
		return programService.findMenus();
	}
	
	@GetMapping(value = "/badge")
	public BadgeMenu badge(@RequestParam(value = "menuId", required = true, name = "menuId") final String menuId) {
		String beanName = "MENU_" + menuId;
		if(application.containsBean(beanName)) {
			Object bean = application.getBean(beanName);
			if(bean instanceof MenuProcessor) {
				MenuProcessor processor = (MenuProcessor) bean;
				return processor.process();
			}
		}
		return null;
	}
	
	
	
}