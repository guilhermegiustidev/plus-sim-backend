package br.com.gst.soft.resource;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.response.ProgramValidationResponse;
import br.com.gst.soft.service.validation.program.ValidationProgram;

@RestController
@RequestMapping("/page/validation")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValidationPageResource implements Serializable{

	private static final long serialVersionUID = -7072370142921477006L;
	
	@Autowired
	private ApplicationContext application;
	
	@GetMapping(value = "/validatePage")
	public ProgramValidationResponse validatePage(@RequestParam(value = "url", required = true, name = "url") final String url) {
		try{
			return ((ValidationProgram) this.application.getBean("VALIDATION_" + url)).validate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	
}
