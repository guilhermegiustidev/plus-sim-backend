package br.com.gst.soft.resource;

import java.io.Serializable;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/psim")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ResourceTest implements Serializable{

	private static final long serialVersionUID = 1311750479318573125L;
	
	@GetMapping("/test")
	public void test() {
		System.out.println("TESTE");
	}

}
