package br.com.gst.soft.resource;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.sim.Lot;
import br.com.gst.soft.entity.sim.LotStatus;
import br.com.gst.soft.pojo.InvoiceLotSearchPojo;
import br.com.gst.soft.response.invoice.lot.ValidationInvoiceLotResponse;
import br.com.gst.soft.service.LotService;

@RestController
@RequestMapping("/lot")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LotResource extends EntityResource<Lot, LotService> {

	private static final long serialVersionUID = 2471162265574284014L;

	@GetMapping(value = "/verifyProgress")
	@PreAuthorize("hasPermission(this, 'READ')")
	public boolean verifyProgress() {
		return getBusiness().verifyProgress();
	}

	@GetMapping(value = "/generateLot")
	@PreAuthorize("hasPermission(this, 'READ')")
	public Lot generateLot() {
		return getBusiness().findLoadInProgress();
	}

	@PreAuthorize("hasPermission(this, 'WRITE')")
	@PostMapping(value = "/done", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Lot done(@RequestBody final Lot lot) {
		LotService service = getBusiness();
		Lot entity = service.findOne(lot.getId());
		entity.setBranch(lot.getBranch());
		if(entity != null && LotStatus.PROGRESS.equals(entity.getStatus())) {
			entity.setStatus(LotStatus.DONE);
			entity = service.merge(entity);
		}
		return entity;
	}
	

	@PreAuthorize("hasPermission(this, 'READ')")
	@PostMapping(value = "/validateLot", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public ValidationInvoiceLotResponse validateLot(@RequestBody final InvoiceLotSearchPojo pojo) {
		return this.getBusiness().validateLot(pojo);
	}
	
	
	
}
