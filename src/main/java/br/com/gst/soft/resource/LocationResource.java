package br.com.gst.soft.resource;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.localization.City;
import br.com.gst.soft.entity.localization.Country;
import br.com.gst.soft.entity.localization.State;
import br.com.gst.soft.pojo.CityCountryDTO;
import br.com.gst.soft.service.CityService;
import br.com.gst.soft.service.CountryService;
import br.com.gst.soft.service.StateService;

@RestController
@RequestMapping("/location")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LocationResource implements Serializable{

	private static final long serialVersionUID = -7798201615891589900L;

	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@GetMapping(value = "/country/findAll")
	public List<Country> findAllCountries() {
		return countryService.findAll();
	}
	
	@GetMapping(value = "/state/findAll")
	public List<State> findAllStates() {
		return stateService.findAll();
	}
	
	@GetMapping(value = "/city/findAll")
	public List<City> findAllCities() {
		return cityService.findAll();
	}
	
	@GetMapping(value = "/country/findById")
	public Country findCountryBy(@RequestParam(value = "id", required = true, name = "id") final Long id) {
		return countryService.findOne(id);
	}
	
	@GetMapping(value = "/state/findById")
	public State findStateById(@RequestParam(value = "id", required = true, name = "id") final Long id) {
		return stateService.findOne(id);
	}
	
	@GetMapping(value = "/city/findById")
	public City findCityById(@RequestParam(value = "id", required = true, name = "id") final Long id) {
		return cityService.findOne(id);
	}
	
	
	@GetMapping(value = "/state/findStatesByCountryId")
	public List<State> findStatesByCountryId(@RequestParam(value = "countryId", required = true, name = "countryId") Long countryId) {
		return stateService.findByCountryId(countryId);
	}
	
	@GetMapping(value = "/city/findCitiesByStateId")
	public List<City> findCitiesByStateId(@RequestParam(value = "stateId", required = true, name = "stateId") Long stateId) {
		return cityService.findByStateId(stateId);
	}

	@PostMapping(value = "/city/findCitiesByCountryIdAndLikeNameAndStatePage", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Page<City> findCitiesByCountryIdAndLikeNameAndStatePage(@RequestBody final CityCountryDTO pojo) {
		Example<City> example = getExampleCityCountry(pojo);
		PageRequest pageable = PageRequest.of(pojo.getPageNumber(), pojo.getPageSize(), Direction.ASC, "id");
		return cityService.page(example, pageable);
	}

	@PostMapping(value = "/city/findCitiesByCountryIdAndLikeNameAndStateCount", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Long findCitiesByCountryIdAndLikeNameCount(@RequestBody final CityCountryDTO entity) throws ReflectiveOperationException {
		Example<City> example = getExampleCityCountry(entity);
		return cityService.getCount(example);
	}

	private Example<City> getExampleCityCountry(final CityCountryDTO entity) {
		City city = new City();
		if(StringUtils.isEmpty(entity.getName()) == false) {
			city.setName(entity.getName());
		}
		State state; 
		if(entity.getStateId() == null) {
			state = new State();
		} else {
			state = stateService.findOne(entity.getStateId());
		}
		state.setCountry(countryService.findOne(entity.getCountryId()));
		city.setState(state);
		Example<City> example = Example.of(city);
		return example;
	}

}
