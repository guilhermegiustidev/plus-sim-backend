package br.com.gst.soft.resource;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.rules.ContactType;
import br.com.gst.soft.service.ContactService;

@RestController
@RequestMapping("/contact")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ContactResource extends EntityResource<Contact, ContactService>{

	private static final long serialVersionUID = 3697918431772761783L;

	@GetMapping(value = "/findContactTypes")
	@PreAuthorize("hasPermission(this, 'READ')")
	public ContactType [] findContactTypes() {
		return ContactType.values();
	}
	
}
