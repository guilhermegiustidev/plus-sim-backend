package br.com.gst.soft.resource;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.secutiry.Profile;
import br.com.gst.soft.response.ModuleProfileDTO;
import br.com.gst.soft.response.ProfilePojo;
import br.com.gst.soft.response.RoleDTO;
import br.com.gst.soft.response.UserProfileDTO;
import br.com.gst.soft.service.ProfileService;

@RestController
@RequestMapping("/profile")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProfileResource extends EntityResource<Profile, ProfileService> {

	private static final long serialVersionUID = 4159028006402447022L;

	@PreAuthorize("hasPermission(this, 'READ')")
	@GetMapping(value = "/findByLikeName")
	public Set<Profile> findByLikeName(@RequestParam(value = "name", required = false, name = "name") final String name) {
		if(!StringUtils.isEmpty(name)) {
			return getBusiness().findByLikeName(name);
		}
		return null;
	}
	
	@GetMapping("/findAllRoles")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<RoleDTO> findAllRoles(
			@RequestParam(value = "idProfile", required = false, name = "idProfile") Long idProfile) {
		return getBusiness().findProfileRoles(idProfile);
	}

	@GetMapping("/findAllUsers")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<UserProfileDTO> findAllUsers(
			@RequestParam(value = "idProfile", required = false, name = "idProfile") Long idProfile) {
		return getBusiness().findAllUsers(idProfile);
	}

	@GetMapping("/findAllPrograms")
	@PreAuthorize("hasPermission(this, 'READ')")
	public Set<String> findAllPrograms(
			@RequestParam(value = "idProfile", required = false, name = "idProfile") Long idProfile) {
		return this.getBusiness().findAllPrograms(idProfile);
	}
	
	@PreAuthorize("hasPermission(this, 'WRITE')")
	@PostMapping(value = "/mergeProfile", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Profile merge(@RequestBody final ProfilePojo entity) {
		return getBusiness().mergeProfile(entity);
	}


}
