package br.com.gst.soft.resource;

import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.servicos.PlanServiceType;
import br.com.gst.soft.service.PlanServiceTypeService;

@RestController
@RequestMapping("/planServiceType")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PlanServiceTypeResource extends EntityResource<PlanServiceType, PlanServiceTypeService>{

	private static final long serialVersionUID = 9091602309026361330L;

	@PreAuthorize("hasPermission(this, 'READ')")
	@GetMapping(value = "/findByLikeName")
	public Set<PlanServiceType> findByLikeName(@RequestParam(value = "name", required = false, name = "name") final String name) {
		if(!StringUtils.isEmpty(name)) {
			return getBusiness().findByLikeName(name);
		}
		return null;
	}
	
}
