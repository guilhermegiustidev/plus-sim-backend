package br.com.gst.soft.resource;

import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.config.ConfigLayout;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.repository.ConfigLayoutRepository;
import br.com.gst.soft.repository.ProfileRepository;
import br.com.gst.soft.response.PersonDTO;
import br.com.gst.soft.security.UserDataSession;
import br.com.gst.soft.service.ProgramService;

@RestController
@RequestMapping("/oauth")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TokenResource {

	@Autowired
	public ProfileRepository profileRepository;

	@Autowired
	private UserDataSession loggedUserComponent;

	@Autowired
	private ConfigLayoutRepository configLayoutRepository;
	
	@GetMapping("/hasPermission")
	public boolean hasPermission(@RequestParam(value = "programCod", required = true, name = "programCod") final String programCod) {
		return true;
	}
	
	@GetMapping("/findUser")
	public PersonDTO findUser() {
		Person person = loggedUserComponent.getUserLogged();
		PersonDTO personDTO = new PersonDTO();
		personDTO.setId(person.getId());
		personDTO.setName(person.getName());
		personDTO.setLogin(person.getLogin());
		personDTO.setAvatar(person.getAvatar());
		Optional<ConfigLayout> config = configLayoutRepository.findByPerson(person);
		
		if(config.isPresent()) {
			personDTO.setConfig(config.get());
		}
		
		return personDTO;
	}

	@DeleteMapping("/revoke")
	public void revoke(HttpServletRequest req, HttpServletResponse res) {
		Cookie cookie = new Cookie("refreshToken", null);
		cookie.setHttpOnly(true);
		cookie.setSecure(true);
		cookie.setPath(req.getContextPath() + "/oauth/token");
		cookie.setMaxAge(0);
		res.addCookie(cookie);
		res.setStatus(HttpStatus.NO_CONTENT.value());
	}


}
