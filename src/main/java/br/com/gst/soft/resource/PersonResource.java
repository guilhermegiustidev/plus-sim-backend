package br.com.gst.soft.resource;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.core.filter.RegexFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.config.ConfigLayout;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.pojo.ContactPojo;
import br.com.gst.soft.pojo.MyAccountPersonPojo;
import br.com.gst.soft.pojo.PersonPojo;
import br.com.gst.soft.pojo.ProfileDTO;
import br.com.gst.soft.response.BranchDTO;
import br.com.gst.soft.response.PartnerDTO;
import br.com.gst.soft.service.BranchService;
import br.com.gst.soft.service.PartnerService;
import br.com.gst.soft.service.PersonService;
import br.com.gst.soft.service.ProfileService;

@RestController
@RequestMapping("/person")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PersonResource extends EntityResource<Person, PersonService>{

	private static final long serialVersionUID = -6616015685696494174L;

	@Autowired
	private BranchService branchService;
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private ProfileService profileService;
	
	@PostMapping(value = "/mergeMyAccountData")
	@PreAuthorize("hasPermission(this, 'WRITE')")
	public Person mergeMyAccountData(@RequestBody final MyAccountPersonPojo entity) {
		return getBusiness().mergeMyAccountData(entity);
	}

	@GetMapping(value = "/findConfig")
	public ConfigLayout findConfig() {
		return getBusiness().findConfig();
	}
	
	@PostMapping(value = "/mergePerson")
	@PreAuthorize("hasPermission(this, 'WRITE')")
	public Person merge(@RequestBody final PersonPojo entity) {
		return getBusiness().mergePerson(entity);
	}
	
	@GetMapping(value = "/verifyLogin")
	@PreAuthorize("hasPermission(this, 'READ')")
	public boolean verifyLogin(@RequestParam(value = "login", required = true, name = "login") final String login) {
		return getBusiness().verifyLogin(login);
	}
	
	@PreAuthorize("hasPermission(this, 'READ')")
	@GetMapping(value = "/findByLikeName")
	public Set<Person> findByLikeName(@RequestParam(value = "name", required = false, name = "name") final String name) {
		if(!StringUtils.isEmpty(name)) {
			return getBusiness().findByLikeName(name);
		}
		return null;
	}
	
	@PreAuthorize("hasAuthority('ROLE_BRANCH_READ')")
	@GetMapping(value = "/findBranchsUser")
	public List<BranchDTO> findBranchsUser(@RequestParam(value = "personId", required = false, name = "personId") final Long personId) {
		return branchService.findBranchsDTOUser(getPerson(personId));
	}

	@PreAuthorize("hasAuthority('ROLE_PARTNER_READ')")
	@GetMapping(value = "/findPartnersUser")
	public List<PartnerDTO> findPartnersUser(@RequestParam(value = "personId", required = false, name = "personId") final Long personId) {
		return partnerService.findPartnersUser(getPerson(personId));
	}

	@PreAuthorize("hasAuthority('ROLE_PARTNER_READ')")
	@GetMapping(value = "/findProfilesUser")
	public List<ProfileDTO> findProfilesUser(@RequestParam(value = "personId", required = false, name = "personId") final Long personId) {
		return profileService.findProfilesUser(getPerson(personId));
	}

	@GetMapping(value = "/findContactUser")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<ContactPojo> findContactUser(@RequestParam(value = "personId", required = true, name = "personId") final Long personId){
		return getBusiness().findContactBranch(personId);
	}
	
	private Person getPerson(final Long personId) {
		return personId != null ? this.getBusiness().findOne(personId) : Person.builder().id(0L).build();
	}
	
}
