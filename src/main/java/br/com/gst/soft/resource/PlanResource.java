package br.com.gst.soft.resource;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.servicos.Plan;
import br.com.gst.soft.rules.LogicInstance;
import br.com.gst.soft.service.PlanService;

@RestController
@RequestMapping("/plan")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PlanResource extends EntityResource<Plan, PlanService>{

	private static final long serialVersionUID = 1955157700170971280L;

	@PreAuthorize("hasPermission(this, 'WRITE')")
	@PostMapping(value = "/merge")
	public Plan merge(@RequestBody final Plan entity) {
		if(entity.getServices() != null) {
			entity.getServices().removeIf(index -> index.isStatus() == false);
		}
		return super.merge(entity);
	}

	
}
