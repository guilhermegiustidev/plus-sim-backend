package br.com.gst.soft.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.servicos.Pabx;
import br.com.gst.soft.entity.servicos.Pabx.PabxStatus;
import br.com.gst.soft.service.PabxService;
import br.com.gst.soft.service.RequestBusiness;

@RestController
@RequestMapping("/pabx")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PabxResource extends EntityResource<Pabx, PabxService>{

	private static final long serialVersionUID = -200806894513491212L;

	@Autowired
	private RequestBusiness requestBusiness;
	
	@PreAuthorize("hasPermission(this, 'READ')")
	@RequestMapping(value = "/page", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Page<Pabx> page(final Pabx entity, final Integer pageNumber, final Integer pageSize, final Long count) throws ReflectiveOperationException {
		Example<Pabx> example = findActives(entity);
		PageRequest pageable = PageRequest.of(pageNumber, pageSize, Direction.DESC, "id");
		Page<Pabx> list = getBusiness().findAll(pageable, example);
		if(list != null && list.getContent() != null) {
			for (Pabx pabx : list.getContent()) {
				pabx.setStatusEnum(requestBusiness.verifyStatusPabx(pabx) ? PabxStatus.BUSY : PabxStatus.FREE);
			}
		}
		return list;
	}
	
}