package br.com.gst.soft.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.pojo.AddressPojo;
import br.com.gst.soft.pojo.ContactPojo;
import br.com.gst.soft.pojo.PartnerPojo;
import br.com.gst.soft.response.PartnerDTO;
import br.com.gst.soft.response.PersonDTO;
import br.com.gst.soft.security.UserDataSession;
import br.com.gst.soft.service.PartnerService;

@RestController
@RequestMapping("/partner")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PartnerResource extends EntityResource<Partner, PartnerService>{

	private static final long serialVersionUID = -2842338627895579914L;

	@Autowired
	private UserDataSession dataSession;
	
	@PostMapping(value = "/mergePartner")
	@PreAuthorize("hasPermission(this, 'WRITE')")
	public Partner merge(@RequestBody final PartnerPojo entity) {
		return getBusiness().mergePartner(entity);
	}
	
	@GetMapping(value = "/findPartnerByUser")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<PartnerDTO> findPartnerByUser(){
		return this.getBusiness().findPartnersUser(dataSession.getUserLogged());
	}
	
	@GetMapping(value = "/findContactPartner")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<ContactPojo> findContactPartner(@RequestParam(value = "partnerId", required = true, name = "partnerId") final Long partnerId){
		return getBusiness().findContactPartner(partnerId);
	}
	
	@GetMapping(value = "/findAddressPartner")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<AddressPojo> findAddressPartner(@RequestParam(value = "partnerId", required = true, name = "partnerId") final Long partnerId){
		return getBusiness().findAddressPartner(partnerId);
	}
	
	@GetMapping(value = "/findUserPartner")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<PersonDTO> findUserPartner(@RequestParam(value = "partnerId", required = true, name = "partnerId") final Long partnerId){
		return getBusiness().findUserPartner(partnerId);
	}
	
}
