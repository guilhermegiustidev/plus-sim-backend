package br.com.gst.soft.resource;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.pojo.AddressPojo;
import br.com.gst.soft.pojo.BranchPojo;
import br.com.gst.soft.pojo.ContactPojo;
import br.com.gst.soft.response.BranchDTO;
import br.com.gst.soft.response.PersonDTO;
import br.com.gst.soft.security.UserDataSession;
import br.com.gst.soft.service.BranchService;

@RestController
@RequestMapping("/branch")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BranchResource extends EntityResource<Branch, BranchService> {

	private static final long serialVersionUID = -428802464342846861L;

	@Autowired
	private UserDataSession userDataSession;

	@PostMapping(value = "/mergeBranch")
	@PreAuthorize("hasPermission(this, 'WRITE')")
	public Branch merge(@RequestBody final BranchPojo entity) {
		return getBusiness().mergeBranch(entity);
	}

	@GetMapping(value = "/findBranchUser")
	@PreAuthorize("hasPermission(this, 'READ')")
	public Set<BranchDTO> findBranchUser() {
		Set<BranchDTO> branchs = new HashSet<BranchDTO>();
		
		getBusiness().findBranchUser(userDataSession.getUserLogged()).forEach(index -> {
			BranchDTO dto = getBusiness().convertModelToDto(index);
			if(index.isStatus()) {
				branchs.add(dto);
			}
		});
		return branchs;
	}

	@GetMapping(value = "/findContactBranch")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<ContactPojo> findContactBranch(
			@RequestParam(value = "branchId", required = true, name = "branchId") final Long branchId) {
		return getBusiness().findContactBranch(branchId);
	}

	@GetMapping(value = "/findAddressBranch")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<AddressPojo> findAddressBranch(
			@RequestParam(value = "branchId", required = true, name = "branchId") final Long branchId) {
		return getBusiness().findAddressBranch(branchId);
	}

	@GetMapping(value = "/findUserBranch")
	@PreAuthorize("hasPermission(this, 'READ')")
	public List<PersonDTO> findUserBranch(
			@RequestParam(value = "branchId", required = true, name = "branchId") final Long branchId) {
		return getBusiness().findUserBranch(branchId);
	}

}
