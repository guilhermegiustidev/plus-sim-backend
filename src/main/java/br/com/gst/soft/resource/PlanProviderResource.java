package br.com.gst.soft.resource;


import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.servicos.PlanProvider;
import br.com.gst.soft.service.PlanProviderService;

@RestController
@RequestMapping("/planProvider")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PlanProviderResource extends EntityResource<PlanProvider, PlanProviderService>{

	private static final long serialVersionUID = 1416031712115938484L;

	@GetMapping(value = "/findPlanProviderByProviderId")
	public Set<PlanProvider> findPlanProviderByProviderId(@RequestParam(value = "id", required = true, name = "id") final Long id){
		return this.getBusiness().findPlanProviderByProviderId(id);
	}

	
}
