package br.com.gst.soft.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.service.ContactService;
import br.com.gst.soft.service.ProviderService;

@RestController
@RequestMapping("/provider")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProviderResource extends EntityResource<Provider, ProviderService>{

	private static final long serialVersionUID = 1751310852892285515L;

	@Autowired
	private ContactService contactService;
	
	@PreAuthorize("hasPermission(this, 'WRITE')")
	@PostMapping(value = "/merge")
	public Provider merge(@RequestBody final Provider entity) {
		entity.setStatus(true);
		if(entity.getContacts() != null) {
			for (Contact index : entity.getContacts()) {
				switch (index.getType()) {
				case EMAIL:
					index.setValue(index.getEmail());
					break;
				case PHONE:
					index.setValue(index.getPhone());
					break;
				}
				Contact merge = contactService.merge(index);
				merge.setValue(index.getValue());
				index.setId(merge.getId());
			}
		}
		return getBusiness().merge(entity);
	}
	
}