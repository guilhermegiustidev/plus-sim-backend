package br.com.gst.soft.resource;

import org.joda.time.DateTime;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/utils")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UtilsResource {

	@GetMapping(value = "/generateHash")
	public Long findContactTypes() {
		return DateTime.now().getMillis();
	}
	
}
