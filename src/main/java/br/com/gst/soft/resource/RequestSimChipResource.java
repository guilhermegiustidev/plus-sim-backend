package br.com.gst.soft.resource;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.service.RequestSimChipService;

@Deprecated
@RestController
@RequestMapping("/request/sim")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestSimChipResource extends EntityResource<RequestSimChip, RequestSimChipService>{

	private static final long serialVersionUID = -1341829954279678729L;

	
	
}
