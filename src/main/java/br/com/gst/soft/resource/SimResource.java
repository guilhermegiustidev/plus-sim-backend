package br.com.gst.soft.resource;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.sim.Lot;
import br.com.gst.soft.entity.sim.LotStatus;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.SimLot;
import br.com.gst.soft.entity.sim.Sim.SimChipStatus;
import br.com.gst.soft.response.SimInvoiceResponse;
import br.com.gst.soft.response.SimResponse;
import br.com.gst.soft.service.LotService;
import br.com.gst.soft.service.ProviderService;
import br.com.gst.soft.service.SimLotService;
import br.com.gst.soft.service.SimService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sim")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimResource extends EntityResource<Sim, SimService> {

	private static final long serialVersionUID = 3489421484407056772L;

	@Autowired
	private ProviderService providerService;

	@Autowired
	private LotService lotService;

	@Autowired
	private SimLotService simLotService;
	
	@PreAuthorize("hasPermission(this, 'READ')")
	@GetMapping(value = "/countSimsLot")
	public Long countSimsLot(@RequestParam(value = "lotId", required = true, name = "lotId") final Long lotId) throws ReflectiveOperationException {
		Example<SimLot> example = Example.of(SimLot.builder().lot(lotService.findOne(lotId)).build());
		return this.simLotService.getCount(example);
	}

	@GetMapping(value = "/findAllSimByLotId")
	public List<Sim> findAllSimByLotId(@RequestParam(value = "lotId", required = true, name = "lotId") final Long lotId){
		Example<SimLot> example = Example.of(SimLot.builder().lot(lotService.findOne(lotId)).build());
		List<SimLot> allSimLot = this.simLotService.findAll(example);
		return allSimLot.stream().collect(Collectors.toList()).stream().map(SimLot::getSim).collect(Collectors.toList());
	}
	
	@Override
	@PreAuthorize("hasPermission(this, 'WRITE')")
	@DeleteMapping(value = "/delete", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public boolean delete(@RequestBody final Sim entity) {
		SimService simBusiness = getBusiness();
		Sim removeSim = simBusiness.findOne(entity.getId());
		if (simBusiness.verifyBeforeExclude(removeSim)) {
			getBusiness().remove(removeSim);
			return true;
		}
		return false;
	}

	@GetMapping(value = "/addSin")
	@PreAuthorize("hasPermission(this, 'WRITE')")
	public SimResponse addSin(@RequestParam(value = "lotId", required = true, name = "lotId") final Long lotId,
			@RequestParam(value = "providerId", required = true, name = "providerId") final Long providerId,
			@RequestParam(value = "code", required = true, name = "code") final String code) {

		Lot lot = lotService.findOne(lotId);

		Optional<Sim> optionalSim = getBusiness().findByCode(code);

		Provider provider = providerService.findOne(providerId);
		if (optionalSim.isPresent()) {
			Sim sim = optionalSim.get();
			Optional<SimLot> simLot = this.simLotService.findBySimAndLot(sim, lot);
			if (simLot.isPresent()) {
				log.warn("Sim ja existe = {}", code);
			} else {
				return buildSimLot(code, lot, provider);				
			}
			return SimResponse.builder().idSim(sim.getId()).code(code).lotId(lotId).lotCode(code).valid(false).build();
		} else {
			Pattern pattern = Pattern.compile(provider.getRegex());
			Matcher matcher = pattern.matcher(code);
			if (matcher.matches()) {
				return buildSimLot(code, lot, provider);
			}
		}
		return null;
	}

	private SimResponse buildSimLot(final String code, Lot lot, Provider provider) {
		Sim sim = Sim.builder().code(code).provider(provider).regex(provider.getRegex()).status(SimChipStatus.FREE).build();
		lot.setStatus(LotStatus.PROGRESS);
		sim = this.getBusiness().merge(sim);
		lot = this.lotService.merge(lot);
		this.simLotService.merge(SimLot.builder().sim(sim).lot(lot).include(DateTime.now().toDate()).build());
		return SimResponse.builder().idSim(sim.getId()).code(code).lotId(lot.getId()).lotCode(lot.getCode()).valid(true).build();
	}
	
	@PreAuthorize("hasPermission(this, 'READ')")
	@GetMapping(value = "/findSimAvailable")
	public SimInvoiceResponse findSimAvailable(
			@RequestParam(value = "code", required = true, name = "code") final String code,
			@RequestParam(value = "branchId", required = true, name = "branchId") final Long branchId) {
		return this.getBusiness().findSimAvailable(code, branchId);
	}
	
	@PreAuthorize("hasPermission(this, 'READ')")
	@DeleteMapping(value = "/removeSims", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public void removeSims(@RequestBody final Set<Sim> entities) {
		for(Sim sim : entities) {
			Sim entity = this.getBusiness().findOne(sim.getId());
			if(entity.getStatus().equals(SimChipStatus.FREE)) {
				Optional<SimLot> optional = this.simLotService.findBySim(entity);
				if(optional.isPresent()) {
					this.simLotService.remove(optional.get());
				}
				this.getBusiness().remove(entity);
			}
		}
	}
	

}