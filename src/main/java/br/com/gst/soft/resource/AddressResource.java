package br.com.gst.soft.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.address.Address;
import br.com.gst.soft.service.AddressService;
import br.com.gst.soft.service.CityService;
import br.com.gst.soft.service.CountryService;
import br.com.gst.soft.service.StateService;

@RestController
@RequestMapping("/address")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AddressResource extends EntityResource<Address, AddressService>{

	private static final long serialVersionUID = 8954997681322781535L;

	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CityService cityService;
	
	@Override
	public Address merge(@RequestBody final Address entity) {
		entity.setCountry(countryService.findOne(entity.getCountry().getId()));
		entity.setState(stateService.findOne(entity.getState().getId()));
		entity.setCity(cityService.findOne(entity.getCity().getId()));
		Address merge = super.merge(entity);
		return merge;
	}
	
}