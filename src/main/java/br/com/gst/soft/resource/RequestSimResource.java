package br.com.gst.soft.resource;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.request.invoice.InvoiceRequestChip;
import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.entity.request.sim.RequestStatus;
import br.com.gst.soft.pojo.InvoiceSimDTO;
import br.com.gst.soft.response.invoice.sim.ValidationInvoiceChipResponse;
import br.com.gst.soft.rules.ContactType;
import br.com.gst.soft.service.InvoiceRequestChipService;
import br.com.gst.soft.service.RequestSimChipService;

@RestController
@RequestMapping("/requests/sim")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestSimResource extends EntityResource<RequestSimChip, RequestSimChipService>{

	private static final long serialVersionUID = 3697918431772761783L;
	
	@Autowired
	private InvoiceRequestChipService invoiceRequestChipService;
	
	@GetMapping(value = "/findContactTypes")
	@PreAuthorize("hasPermission(this, 'READ')")
	public ContactType [] findContactTypes() {
		return ContactType.values();
	}

	@GetMapping(value = "/findInvoice")
	@PreAuthorize("hasPermission(this, 'READ')")
	public InvoiceRequestChip findInvoice(@RequestParam(value = "requestId", required = true, name = "requestId") Long requestId) {
		RequestSimChip requestSimChip = this.getBusiness().findOne(requestId);
		Example<InvoiceRequestChip> example = Example.of(InvoiceRequestChip.builder().request(requestSimChip).build(), ExampleMatcher.matchingAny());
		Optional<InvoiceRequestChip> optional = this.invoiceRequestChipService.findOne(example);
		return optional.get();
	}
	
	@GetMapping(value = "/validateRequest")
	@PreAuthorize("hasPermission(this, 'READ')")
	public ValidationInvoiceChipResponse validateRequest(
			@RequestParam(value = "requestId", required = true, name = "requestId") final Long requestId, 
			@RequestParam(value = "branchId", required = true, name = "branchId") final Long branchId) {
		return this.getBusiness().validateRequestSimItems(requestId, branchId);
	}
	
	@PreAuthorize("hasPermission(this, 'READ')")
	@PostMapping(value = "/proccessRequest", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public InvoiceRequestChip proccessRequest(@RequestBody InvoiceSimDTO dto) {
		return this.invoiceRequestChipService.proccessRequest(dto);
	}
	
	@Override
	protected Page<RequestSimChip> page(PageRequest pageable, Example<RequestSimChip> example) {
		Page<RequestSimChip> page = getBusiness().findAll(pageable, example);
		getBusiness().buildDataPage(page);
		return page;
	}

	@Override
	public Long count(@RequestBody final RequestSimChip entity) throws ReflectiveOperationException {
		Example<RequestSimChip> example = findActives(entity);
		return count(example);
	}


	
}
