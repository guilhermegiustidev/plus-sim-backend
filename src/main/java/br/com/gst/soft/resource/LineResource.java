package br.com.gst.soft.resource;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gst.soft.entity.line.Line;
import br.com.gst.soft.service.LineService;

@RestController
@RequestMapping("/line")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LineResource extends EntityResource<Line, LineService>{

	private static final long serialVersionUID = 4746865396396283883L;

}
