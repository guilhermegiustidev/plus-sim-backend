package br.com.gst.soft.resource;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.rules.HasGenericalImplementation;
import br.com.gst.soft.rules.LogicInstance;
import br.com.gst.soft.service.AbstractService;
import lombok.AccessLevel;
import lombok.Getter;

@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class EntityResource<T extends Model, B extends AbstractService>
		implements HasGenericalImplementation, Serializable {

	private static final long serialVersionUID = 2682099468002479848L;

	@Autowired
	private ApplicationContext application;

	@Getter(value = AccessLevel.PROTECTED)
	private B business;

	@Getter
	private Class<T> clazz;

	@PostConstruct
	public void init() {
		clazz = (Class<T>) ((ParameterizedType) (getClass().getGenericSuperclass())).getActualTypeArguments()[0];
		Class<B> clazzBusiness = (Class<B>) ((ParameterizedType) (getClass().getGenericSuperclass()))
				.getActualTypeArguments()[1];
		business = (B) application.getBean(clazzBusiness);
	}

	@GetMapping(value = "/hash")
	public String getHash() {
		return clazz.getSimpleName().concat("_" + DateTime.now().getMillis());
	}
	
	@PreAuthorize("hasPermission(this, 'WRITE')")
	@PostMapping(value = "/merge")
	public T merge(@RequestBody final T entity) {
		if (entity instanceof LogicInstance) {
			((LogicInstance) entity).exclude();
			((LogicInstance) entity).setStatus(true);
		}
		return (T) business.merge(entity);
	}

	@PreAuthorize("hasPermission(this, 'WRITE')")
	@DeleteMapping(value = "/delete")
	public boolean delete(@RequestBody final T entity) {
		T removeValue = (T) business.findOne(entity.getId());
		if (removeValue instanceof LogicInstance) {
			((LogicInstance) removeValue).exclude();
			business.merge(removeValue);
		} else {
			business.remove(removeValue);
		}
		return true;
	}


	@PreAuthorize("hasPermission(this, 'WRITE')")
	@DeleteMapping(value = "/deleteAll", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public boolean deleteAll(@RequestBody final List<T> body) {
		for(T ent : body) {
			Serializable entity = business.findOne(ent.getId());
			if (entity instanceof LogicInstance) {
				((LogicInstance) entity).exclude();
				business.merge(entity);
			} else {
				business.remove(ent);
			}
		}
		return true;
	}

	@PreAuthorize("hasPermission(this, 'READ')")
	@PostMapping(value = "/count", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Long count(@RequestBody final T entity) throws ReflectiveOperationException {
		Example<T> example = findActives(entity);
		return count(example);
	}


	@PreAuthorize("hasPermission(this, 'READ')")
	@RequestMapping(value = "/page", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Page<T> page(final T entity, final Integer pageNumber, final Integer pageSize, final Long count) throws ReflectiveOperationException {
		Example<T> example = findActives(entity);
		PageRequest pageable = PageRequest.of(pageNumber, pageSize, Direction.DESC, "id");
		return page(pageable, example);
	}

	protected Long count(Example<T> example) {
		return business.getCount(example);
	}

	protected Page<T> page(PageRequest pageable, Example<T> example) {
		return business.findAll(pageable, example);
	}

	@PreAuthorize("hasPermission(this, 'READ')")
	@GetMapping("/findAll")
	public List<T> findAll() {
		return business.findAll();
	}

	@PreAuthorize("hasPermission(this, 'READ')")
	@RequestMapping(value = "/findById", method = RequestMethod.GET)
	public T findById(@RequestParam(value = "id", required = true, name = "id") final Long id) {
		return (T) business.findOne(id);
	}

	protected Example<T> findActives(final T entity) throws InstantiationException, IllegalAccessException {
		Example<T> example = null;
		T filter = entity;
		if (LogicInstance.class.isAssignableFrom(clazz)) {
			if (filter == null) {
				filter = clazz.newInstance();
			}
			ExampleMatcher matcher = ExampleMatcher.matchingAny();
			((LogicInstance) filter).setStatus(true);
			example = Example.of(filter, matcher);
		}
		return example;
	}
	
}
