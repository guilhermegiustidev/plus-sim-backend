package br.com.gst.soft.menu;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BadgeMenu implements Serializable {

	private static final long serialVersionUID = -2531634215451975535L;
	private Integer title;
	private Integer translate;
	private String bg;
	private String fg;
	
	public static BadgeMenu empty() {
		return BadgeMenu.builder().title(0).translate(0).build();
	}
	
	public BadgeMenu add(BadgeMenu bagde) {
		if(bagde != null) {
			if(this.title == null) {
				this.title = 0;
			}
			if(this.translate == null) {
				this.translate = 0;
			}

			if(bagde.getTitle() != null) {
				this.title+=bagde.getTitle();
			}
			
			if(bagde.getTranslate() != null) {
				this.translate+=bagde.getTranslate();
			}
		}
		return this;
	}
	
}
