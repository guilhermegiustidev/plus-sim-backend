package br.com.gst.soft.menu;

public interface MenuProcessor {

	public BadgeMenu process();
	
}
