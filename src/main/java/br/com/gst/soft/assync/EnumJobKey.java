package br.com.gst.soft.assync;

public interface EnumJobKey {

	public Long getMaxTry();
	public boolean isEnable();

	
}
