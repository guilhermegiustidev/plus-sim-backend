package br.com.gst.soft.assync;

public interface RunnableJob <T> {

	public EnumJobKey getKey();
	public void execute();
}
