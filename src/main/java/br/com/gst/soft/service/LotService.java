package br.com.gst.soft.service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.request.sim.RequestItemSimChip;
import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.secutiry.Token;
import br.com.gst.soft.entity.sim.Lot;
import br.com.gst.soft.entity.sim.LotStatus;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.Sim.SimChipStatus;
import br.com.gst.soft.entity.sim.SimLot;
import br.com.gst.soft.pojo.InvoiceLotSearchPojo;
import br.com.gst.soft.repository.LotRepository;
import br.com.gst.soft.repository.RequestSimChipRepository;
import br.com.gst.soft.repository.SimLotRepository;
import br.com.gst.soft.repository.SimRepository;
import br.com.gst.soft.response.SimResponse;
import br.com.gst.soft.response.invoice.lot.ValidationInvoiceLotResponse;
import br.com.gst.soft.security.UserDataSession;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LotService extends AbstractService<Lot, LotRepository> {

	@Autowired
	private UserDataSession loggerUser;
	
	@Autowired
	private SimRepository simRepository;

	@Autowired
	private SimLotRepository simLotRepository;
	
	@Autowired
	private RequestSimChipRepository requestSimChipRepository;
	
	public Lot findLoadInProgress() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String code = sdf.format(DateTime.now().toDate());
		Person person = loggerUser.getUserLogged();
		Lot lot = new Lot();
		lot.setCode(code.concat("U").concat(person.getId().toString()));
		lot.setStatus(LotStatus.PROGRESS);
		lot.setPerson(person);
		return this.merge(lot);
	}

	public boolean verifyProgress() {
		return this.findPersonLotByStatus(LotStatus.PROGRESS).isPresent();
	}

	public Optional<Lot> findPersonLotByStatus(LotStatus status){
		Person person = loggerUser.getUserLogged();
		Optional<Lot> optional = getRepository().findByPersonAndStatus(person, status);
		return optional;
	
	}
	
	public void removeLotByStatus(LotStatus status) {
		Optional<Lot> optional = this.findPersonLotByStatus(status);
		if(optional.isPresent()) {
			Lot lot = optional.get();
			List<SimLot> simsLot = simLotRepository.findByLot(lot);
			if(CollectionUtils.isNotEmpty(simsLot)) {
				List<Sim> sims = simsLot.stream().collect(Collectors.toList()).stream().map(SimLot::getSim).collect(Collectors.toList());
				if(CollectionUtils.isNotEmpty(sims)) {
					simRepository.deleteAll(sims);
				}
				simLotRepository.deleteAll(simsLot);
			}
			this.remove(lot);
		}
	}

	public ValidationInvoiceLotResponse validateLot(InvoiceLotSearchPojo pojo) {
		ValidationInvoiceLotResponse response = ValidationInvoiceLotResponse.builder().build();

		Optional<RequestSimChip> optionalRequest = this.requestSimChipRepository.findById(pojo.getRequestId());

		RequestSimChip request = optionalRequest.orElseThrow(() -> new RuntimeException("Erro ao obter Id do Pedido"));
		
		if(CollectionUtils.isNotEmpty(pojo.getSims())) {
			pojo.getSims().forEach(index -> response.addSim(index));
		}
		
		Optional<Lot> optional = this.getRepository().findByCode(pojo.getLotCode());
		if(optional.isPresent()) {
			Lot lot = optional.get();
			if(lot.getBranch().getId().equals(pojo.getBranchId()) == false ) {
				response.notInFilial();
				return response;
			}
			List<SimLot> sims = this.simLotRepository.findByLot(lot);
			if(CollectionUtils.isNotEmpty(sims)) {
				for (SimLot index : sims) {
					Sim sim = index.getSim();
					if(SimChipStatus.FREE.equals(sim.getStatus())) {
						SimResponse simResponse = SimResponse.builder()
								.idSim(sim.getId()).code(sim.getCode()).lotId(lot.getId())
								.lotCode(lot.getCode()).valid(true).providerId(sim.getProvider().getId()).build();
						response.addSim(simResponse);
					}
				}
			}
		}
		
		if(CollectionUtils.isNotEmpty(request.getItems())) {
			request.getItems().forEach(index -> response.processItem(index));
		}
		return response.finalizeLot();
	}
	
}