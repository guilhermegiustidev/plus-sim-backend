package br.com.gst.soft.service;

import java.util.Optional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.line.Line;
import br.com.gst.soft.entity.line.LineStatus;
import br.com.gst.soft.entity.localization.City;
import br.com.gst.soft.entity.localization.Country;
import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.servicos.PlanProvider;
import br.com.gst.soft.repository.CityRepository;
import br.com.gst.soft.repository.CountryRepository;
import br.com.gst.soft.repository.LineRepository;
import br.com.gst.soft.repository.PlanProviderRepository;
import br.com.gst.soft.repository.ProviderRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LineService extends AbstractService<Line, LineRepository> {

	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private ProviderRepository providerRepository;
	
	@Autowired
	private PlanProviderRepository planProviderRepository; 
	
	@Override
	public Line merge(Line entity) {
		Line merge = null;
		if(entity.getId() == null) {
			merge = new Line();
			merge.setReleased(DateTime.now().toDate());
			merge.setStatus(LineStatus.FREE);
		} else {
			merge = findOne(entity.getId());
		}
		merge.setCity(entity.getCity());
		merge.setProvider(entity.getProvider());
		merge.setProviderPlan(entity.getProviderPlan());
		if(entity != null && entity.getCountry() != null) {
			Optional<Country> country = countryRepository.findById(entity.getCountry().getId());
			country.orElseThrow(() -> new RuntimeException("Erro ao obter país da linha"));
			merge.setCountry(country.get());
		}
		if(entity != null && entity.getCity() != null) {
			Optional<City> city = cityRepository.findById(entity.getCity().getId());
			city.orElseThrow(() -> new RuntimeException("Erro ao obter cidade da linha"));
			merge.setCity(city.get());
		}
		if(entity != null && entity.getProvider() != null) {
			Optional<Provider> provider = providerRepository.findById(entity.getProvider().getId());
			provider.orElseThrow(() -> new RuntimeException("Erro ao obter fornecedor da linha"));
			merge.setProvider(provider.get());
		}
		if(entity != null && entity.getProviderPlan() != null) {
			Optional<PlanProvider> planProvider = planProviderRepository.findById(entity.getProviderPlan().getId());
			planProvider.orElseThrow(() -> new RuntimeException("Erro ao obter Plano da linha"));
			merge.setProviderPlan(planProvider.get());
		}
		
		merge.setNumber(entity.getNumber());
		return getRepository().save(merge);
	}
	
	
}
