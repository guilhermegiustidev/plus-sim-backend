package br.com.gst.soft.service.validation.program;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.servicos.PlanServiceType;
import br.com.gst.soft.response.ProgramValidationResponse;
import br.com.gst.soft.service.PlanServiceTypeService;

@Service("VALIDATION_ADM05")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValidationPlusSimPlanPage implements ValidationProgram{

	@Autowired
	private PlanServiceTypeService planServiceTypeService;
	
	@Override
	public ProgramValidationResponse validate() {
		
		ProgramValidationResponse response = new ProgramValidationResponse();
		response.setCanNavigate(true);
		
		ExampleMatcher matcher = ExampleMatcher.matchingAny();
		PlanServiceType entity = new PlanServiceType();
		entity.setStatus(true);
		Example<PlanServiceType> exampleProvider = Example.of(entity, matcher);
		List<PlanServiceType> allActiveProvider = planServiceTypeService.findAll(exampleProvider);
		
		if(CollectionUtils.isEmpty(allActiveProvider)) {
			response.setCanNavigate(false);
			response.setLabels("PLAN.ERROR.DEPENDENCIES");
		}
		
		return response;
	}

}
