package br.com.gst.soft.service.menu;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.entity.request.sim.RequestStatus;
import br.com.gst.soft.menu.BadgeMenu;
import br.com.gst.soft.menu.MenuProcessor;
import br.com.gst.soft.repository.RequestSimChipRepository;

@Service("MENU_ORD04")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ManagerPartnerOrdersChipMenu implements MenuProcessor{

	@Autowired
	private RequestSimChipRepository repository;
	
	@Override
	public BadgeMenu process() {
		List<RequestSimChip> requestsOpened = repository.findByStatus(RequestStatus.OPENED);
		if(CollectionUtils.isEmpty(requestsOpened)) {
			return BadgeMenu.empty();
		}
		int size = requestsOpened.size();
		return BadgeMenu.builder().fg("#FFFFFF").title(size).translate(size).bg("#F44336").build();
	}

}
