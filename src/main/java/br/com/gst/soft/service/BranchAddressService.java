package br.com.gst.soft.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.gst.soft.entity.branch.BranchAddress;
import br.com.gst.soft.repository.BranchAddressRepository;
import br.com.gst.soft.rules.AddressDependencyService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BranchAddressService extends AbstractService<BranchAddress, BranchAddressRepository> implements AddressDependencyService<BranchAddress> {

	public List<BranchAddress> findByBranch(Long branchId) {
		return this.getRepository().findByDependency(branchId);
	}

	@Override
	public BranchAddress findByAddressAndDependency(Long addressId, Long dependencyId) {
		Optional<BranchAddress> optional = this.getRepository().findByAddressAndBranch(addressId, dependencyId);
		return optional.isPresent() ? optional.get() : new BranchAddress();
	}

	@Transactional(propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	public BranchAddress mergeDependency(BranchAddress entity) {
		return merge(entity);
	}

}
