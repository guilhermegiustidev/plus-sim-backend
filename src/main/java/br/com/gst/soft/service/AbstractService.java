package br.com.gst.soft.service;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import lombok.AccessLevel;
import lombok.Getter;

public abstract class AbstractService<E extends Serializable, R extends JpaRepository<E, Long>> {

	@Autowired
    private ApplicationContext application;
	
	@Getter(value=AccessLevel.PROTECTED)
	private R repository;
	
	@Getter(value=AccessLevel.PROTECTED)
	private Class<E> clazz = null;
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {
		this.clazz = (Class<E>) ((ParameterizedType) (getClass().getGenericSuperclass())).getActualTypeArguments()[0];
		Class<R> clazzRepository = (Class<R>) ((ParameterizedType) (getClass().getGenericSuperclass())).getActualTypeArguments()[1];
		repository = (R) application.getBean(clazzRepository);
	}

	public Page<E> page(Example<E> example, Pageable pageable){
		return repository.findAll(example, pageable);
	}
	
	public Long getCount(Example<E> example) {
		if(example != null) {
			return repository.count(example);
		}
		return repository.count();
	}
	
	public List<E> findAll(){
		return repository.findAll();
	}
	
	public List<E> findAll(Example<E> example){
		return repository.findAll(example);
	}
	
	public E merge(E entity){
		return repository.save(entity);
	}
	
	public E findOne(Long id){
		return repository.getOne(id);
	}

	public Optional<E> findOne(Example<E> example){
		return repository.findOne(example);
	}
	
	public void remove(E entity){
		repository.delete(entity);
	}
	
	public Page<E> findAll(Pageable pageable, Example<E> example){
		if(example != null) {
			return repository.findAll(example, pageable);
		} else {
			return repository.findAll(pageable);
		}
	}

	
}
