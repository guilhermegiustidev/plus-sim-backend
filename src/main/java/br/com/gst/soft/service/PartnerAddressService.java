package br.com.gst.soft.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.gst.soft.entity.partner.PartnerAddress;
import br.com.gst.soft.repository.PartnerAddressRepository;
import br.com.gst.soft.rules.AddressDependencyService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PartnerAddressService extends AbstractService<PartnerAddress, PartnerAddressRepository> implements AddressDependencyService<PartnerAddress> {

	public List<PartnerAddress> findByBranch(Long branchId) {
		return this.getRepository().findByDependency(branchId);
	}

	@Override
	public PartnerAddress findByAddressAndDependency(Long addressId, Long dependencyId) {
		Optional<PartnerAddress> optional = this.getRepository().findByAddressAndPartner(addressId, dependencyId);
		return optional.isPresent() ? optional.get() : new PartnerAddress();
	}

	@Transactional(propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	public PartnerAddress mergeDependency(PartnerAddress entity) {
		return merge(entity);
	}

}
