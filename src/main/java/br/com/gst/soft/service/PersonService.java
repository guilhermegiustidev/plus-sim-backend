package br.com.gst.soft.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.config.ColorClasses;
import br.com.gst.soft.entity.config.ConfigLayout;
import br.com.gst.soft.entity.config.Layout;
import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.pojo.ContactPojo;
import br.com.gst.soft.pojo.MyAccountPersonPojo;
import br.com.gst.soft.pojo.PersonPojo;
import br.com.gst.soft.repository.BranchRepository;
import br.com.gst.soft.repository.ConfigLayoutRepository;
import br.com.gst.soft.repository.PartnerRepository;
import br.com.gst.soft.repository.UserRepository;
import br.com.gst.soft.response.BranchDTO;
import br.com.gst.soft.response.PartnerDTO;
import br.com.gst.soft.response.PersonDTO;
import br.com.gst.soft.rules.HasUsers;
import br.com.gst.soft.security.UserDataSession;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PersonService extends AbstractService<Person, UserRepository> {

	@Autowired
	private ContactService contactService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private PartnerRepository partnerRepository;

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private ConfigLayoutRepository configLayoutRepository;
	
	@Autowired
	private UserDataSession loggedUserComponent;
	
	public Optional<Person> findUserByLogin(String login) {
		return this.getRepository().findByLogin(login);
	}

	public Set<Person> findByLikeName(String name) {
		return getRepository().findByLikeName(name);
	}

	@Transactional(propagation = Propagation.MANDATORY, rollbackFor = Exception.class)
	public void mergeUsers(HasUsers merge, List<PersonDTO> listUsers) {
		if (CollectionUtils.isEmpty(listUsers) == false) {
			Map<Long, Person> personMap = new HashMap<Long, Person>();
			for (PersonDTO person : listUsers) {
				if (personMap.containsKey(person.getId()) == false) {
					Optional<Person> personOpt = this.getRepository().findById(person.getId());
					if (personOpt.isPresent()) {
						personMap.put(person.getId(), personOpt.get());
					}
				}
			}
			if (merge.getPersons() == null) {
				merge.setPersons(new HashSet<Person>());
			}
			for (PersonDTO index : listUsers) {
				if (personMap.containsKey(index.getId())) {
					if (!index.isStatus()) {
						merge.getPersons().remove(personMap.get(index.getId()));
					} else {
						merge.getPersons().add(personMap.get(index.getId()));
					}
				}
			}
		}
	}

	public PersonDTO convertEntityToPojo(Person person) {
		PersonDTO pojo = new PersonDTO();
		pojo.setId(person.getId());
		pojo.setName(person.getName());
		pojo.setStatus(person.isStatus());
		return pojo;
	}

	@Transactional(rollbackFor = Exception.class)
	public Person mergePerson(PersonPojo entity) {
		Person merge = null;
		if (entity.getPersonId() != null) {
			merge = this.findOne(entity.getPersonId());
		}
		if (merge == null) {
			merge = new Person();
			PasswordEncoder encoder = new BCryptPasswordEncoder();
			merge.setPassword(encoder.encode("PlusSim2018"));
			merge.setStatus(true);
		}

		merge.setId(entity.getPersonId());
		merge.setLogin(entity.getPersonLogin());
		merge.setName(entity.getPersonName());

		merge = super.merge(merge);

		contactService.mergeContacts(merge, entity.getContacts());

		profileService.mergeProfilesUser(merge, entity.getProfiles());

		this.mergeBranchUser(merge, entity.getBranchs());

		this.mergePartnerUser(merge, entity.getPartners());

		Optional<ConfigLayout> config = configLayoutRepository.findByPerson(merge);
		if(config.isPresent() == false) {
			configLayoutRepository.save(getConfigDefault(merge));
		}
		
		return merge;
	}

	private ConfigLayout getConfigDefault(Person merge) {
		ConfigLayout entityConfig = new ConfigLayout();
		entityConfig.setColorClasses(new ColorClasses());
		entityConfig.setLayout(new Layout());
		entityConfig.setRouterAnimation("slideUp");
		
		entityConfig.getColorClasses().setFooter("mat-fuse-dark-900-bg");
		entityConfig.getColorClasses().setNavbar("mat-fuse-dark-700-bg");
		entityConfig.getColorClasses().setToolbar("mat-white-500-bg");
		entityConfig.getLayout().setFooter("below");
		entityConfig.getLayout().setMode("fullwidth");
		entityConfig.getLayout().setNavigation("left");
		entityConfig.getLayout().setNavigationFolded(false);
		entityConfig.getLayout().setToolbar("below");
				
		entityConfig.setPerson(merge);
		return entityConfig;
	}
	
	public void mergePartnerUser(Person merge, List<PartnerDTO> partners) {
		Map<Long, Partner> map = new HashMap<Long, Partner>();
		if(CollectionUtils.isEmpty(partners) == false) {
			for (PartnerDTO index : partners) {
				Long id = index.getId();
				Optional<Partner> partnerOpt = partnerRepository.findById(id);
				if(partnerOpt.isPresent()) {
					map.put(id, partnerOpt.get());
				}
			}
			for (PartnerDTO index : partners) {
				Long id = index.getId();
				if(map.containsKey(id)) {
					Partner partner = map.get(id);
					if(partner.getPersons() == null) {
						partner.setPersons(new HashSet<Person>());
					}
					if(index.isStatus()) {
						partner.getPersons().add(merge);
					} else {
						partner.getPersons().remove(merge);
					}
				}
			}
		}		
	}
	
	public void mergeBranchUser(Person merge, List<BranchDTO> branchs) {
		Map<Long, Branch> map = new HashMap<Long, Branch>();
		if(CollectionUtils.isEmpty(branchs) == false) {
			for (BranchDTO index : branchs) {
				Long id = index.getId();
				Optional<Branch> branchOpt = branchRepository.findById(id);
				if(branchOpt.isPresent()) {
					map.put(id, branchRepository.findById(id).get());
				}
			}
			for (BranchDTO index : branchs) {
				Long id = index.getId();
				if(map.containsKey(id)) {
					Branch branch = map.get(id);
					if(branch.getPersons() == null) {
						branch.setPersons(new HashSet<Person>());
					}
					if(index.isStatus()) {
						branch.getPersons().add(merge);
					} else {
						branch.getPersons().remove(merge);
					}
				}
			}
		}
	}

	
	public boolean verifyLogin(String login) {
		return this.getRepository().verifyLogin(login);
	}

	public List<ContactPojo> findContactBranch(Long userId) {
		List<ContactPojo> list = new ArrayList<ContactPojo>();
		Person person = findOne(userId);
		if (person != null && CollectionUtils.isEmpty(person.getContacts()) == false) {
			for (Contact contact : person.getContacts()) {
				list.add(contactService.convertEntityToPojo(contact));
			}
		}
		return list;
	}

	public ConfigLayout findConfig() {
		Person userLogged = loggedUserComponent.getUserLogged();
		Optional<ConfigLayout> findByPerson = configLayoutRepository.findByPerson(userLogged);
		if(findByPerson.isPresent()) {
			return findByPerson.get();
		}
		return getConfigDefault(userLogged);
	}

	public Person mergeMyAccountData(MyAccountPersonPojo entity) {
		Person person = loggedUserComponent.getUserLogged();
		configLayoutRepository.save(entity.getConfig());
		return person;
	}

}
