package br.com.gst.soft.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.entity.partner.PartnerAddress;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.pojo.AddressPojo;
import br.com.gst.soft.pojo.ContactPojo;
import br.com.gst.soft.pojo.PartnerPojo;
import br.com.gst.soft.repository.PartnerRepository;
import br.com.gst.soft.response.PartnerDTO;
import br.com.gst.soft.response.PersonDTO;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PartnerService extends AbstractService<Partner, PartnerRepository> {

	@Autowired
	private ContactService contactService;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private PartnerAddressService partnerAddressService;

	@Autowired
	private PersonService personService;
	
	@Transactional(rollbackFor=Exception.class)
	public Partner mergePartner(PartnerPojo entity) {
		Partner merge = null;
		if(entity.getPartnerId() != null) {
			merge = this.findOne(entity.getPartnerId());
		}
		if(merge == null) {
			merge = new Partner();
		}
		merge.setId(entity.getPartnerId());
		merge.setName(entity.getPartnerName());
		merge.setStatus(true);
		
		merge = super.merge(merge);
		
		contactService.mergeContacts(merge, entity.getContacts());
		addressService.mergeAddressess(entity.getAddresses(), partnerAddressService, merge.getId());
		personService.mergeUsers(merge, entity.getUsers());
		
		return merge;
	}

	public List<ContactPojo> findContactPartner(Long partnerId) {
		List<ContactPojo> list = new ArrayList<ContactPojo>();
		Partner partner = findOne(partnerId);
		if(partner != null && CollectionUtils.isEmpty(partner.getContacts()) == false) {
			for (Contact contact : partner.getContacts()) {
				list.add(contactService.convertEntityToPojo(contact));
			}
		}
		return list;
	}

	public List<AddressPojo> findAddressPartner(Long partnerId) {
		List<AddressPojo> addresses = new ArrayList<AddressPojo>();
		List<PartnerAddress> branchAddresses = partnerAddressService.findByBranch(partnerId);
		if(CollectionUtils.isEmpty(branchAddresses) == false) {
			for (PartnerAddress index : branchAddresses) {
				AddressPojo pojo = addressService.findPojoAddress(index.getAddress());
				pojo.setPrimary(index.isPrincipal());
				addresses.add(pojo);
			}
		}
		
		return addresses;
	}

	public List<PersonDTO> findUserPartner(Long partnerId) {
		List<PersonDTO> persons = new ArrayList<PersonDTO>();
		Partner partner = findOne(partnerId);
		if(partner != null && CollectionUtils.isEmpty(partner.getPersons()) == false) {
			for (Person person : partner.getPersons()) {
				persons.add(personService.convertEntityToPojo(person));
			}
		}
		
		return persons;
	}

	public List<PartnerDTO> findPartnersUser(Person person) {
		Map<Long, PartnerDTO> map = new HashMap<Long, PartnerDTO>();
		Set<Partner> partners = this.getRepository().findByPersons(person);
		if(partners == null) {
			partners = new HashSet<Partner>();
		}
		
		partners.forEach(index -> {
			PartnerDTO dto = convertModelToDto(index);
			dto.setStatus(true);
			map.put(dto.getId(), dto);
		});
		
		List<Partner> allPartners = this.findAll();
		
		allPartners.forEach(index -> {
			PartnerDTO dto = convertModelToDto(index);
			dto.setStatus(false);
			if(map.containsKey(dto.getId()) == false) {
				map.put(dto.getId(), dto);
			}
		});
		List<PartnerDTO> list = new ArrayList<PartnerDTO>();
		list.addAll(map.values());
		return list;
	}

	private PartnerDTO convertModelToDto(Partner index) {
		PartnerDTO dto = new PartnerDTO();
		dto.setId(index.getId());
		dto.setName(index.getName());
		return dto;
	}


}
