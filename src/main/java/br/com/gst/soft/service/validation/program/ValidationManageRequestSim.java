package br.com.gst.soft.service.validation.program;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.Sim.SimChipStatus;
import br.com.gst.soft.response.ProgramValidationResponse;
import br.com.gst.soft.security.UserDataSession;
import br.com.gst.soft.service.BranchService;
import br.com.gst.soft.service.SimService;

@Service("VALIDATION_ORD04")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValidationManageRequestSim implements ValidationProgram {

	@Autowired
	private UserDataSession userSessionData;
	
	@Autowired
	private BranchService branchService;
	
	@Autowired
	private SimService simService;
	
	@Override
	public ProgramValidationResponse validate() {
		if(CollectionUtils.isEmpty(this.branchService.findBranchsDTOUser(userSessionData.getUserLogged()))) {
			return ProgramValidationResponse.builder().canNavigate(false).labels("MANAGE_REQUEST_SIM.ERROR.EMPTY_BRANCHS_USER").build();			
		}

		Example<Sim> example = Example.of(Sim.builder().status(SimChipStatus.FREE).build(), ExampleMatcher.matchingAny());
		if(CollectionUtils.isEmpty(this.simService.findAll(example))) {
			return ProgramValidationResponse.builder().canNavigate(false).labels("MANAGE_REQUEST_SIM.ERROR.THERE_NO_SIM_FREE").build();			
		}
		return ProgramValidationResponse.builder().canNavigate(true).build();			
	}

}
