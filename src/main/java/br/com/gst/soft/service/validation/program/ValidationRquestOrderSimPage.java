package br.com.gst.soft.service.validation.program;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.response.PartnerDTO;
import br.com.gst.soft.response.ProgramValidationResponse;
import br.com.gst.soft.security.UserDataSession;
import br.com.gst.soft.service.PartnerService;

@Service("VALIDATION_ORD02")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValidationRquestOrderSimPage implements ValidationProgram{

	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private UserDataSession dataSession;
	
	@Override
	public ProgramValidationResponse validate() {
		Person person = dataSession.getUserLogged();
		List<PartnerDTO> partners = partnerService.findPartnersUser(person);
		
		ProgramValidationResponse response = new ProgramValidationResponse();
		if(CollectionUtils.isEmpty(partners) == false) {
			response.setCanNavigate(true);
		}
		response.setLabels("REQUEST_SIM.ERROR.NAVIGATE.NO_PARTNERS_USER");
		
		return response;
	}

}
