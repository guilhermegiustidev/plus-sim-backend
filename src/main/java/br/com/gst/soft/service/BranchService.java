package br.com.gst.soft.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.branch.BranchAddress;
import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.pojo.AddressPojo;
import br.com.gst.soft.pojo.BranchPojo;
import br.com.gst.soft.pojo.ContactPojo;
import br.com.gst.soft.repository.BranchRepository;
import br.com.gst.soft.response.BranchDTO;
import br.com.gst.soft.response.PersonDTO;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BranchService extends AbstractService<Branch, BranchRepository> {

	@Autowired
	private ContactService contactService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private BranchAddressService branchAddressService;

	@Autowired
	private PersonService personService;

	@Transactional(rollbackFor = Exception.class)
	public Branch mergeBranch(BranchPojo entity) {
		Branch merge = null;
		if (entity.getBranchId() != null) {
			merge = this.findOne(entity.getBranchId());
		}
		if (merge == null) {
			merge = new Branch();
		}
		merge.setId(entity.getBranchId());
		merge.setName(entity.getBranchName());
		merge.setStatus(true);

		merge = super.merge(merge);

		contactService.mergeContacts(merge, entity.getContacts());
		addressService.mergeAddressess(entity.getAddresses(), branchAddressService, merge.getId());
		personService.mergeUsers(merge, entity.getUsers());

		return merge;
	}

	public List<AddressPojo> findAddressBranch(Long branchId) {
		List<AddressPojo> addresses = new ArrayList<AddressPojo>();
		List<BranchAddress> branchAddresses = branchAddressService.findByBranch(branchId);
		if (CollectionUtils.isEmpty(branchAddresses) == false) {
			for (BranchAddress index : branchAddresses) {
				AddressPojo pojo = addressService.findPojoAddress(index.getAddress());
				pojo.setPrimary(index.isPrincipal());
				addresses.add(pojo);
			}
		}

		return addresses;
	}

	public List<ContactPojo> findContactBranch(Long branchId) {
		List<ContactPojo> list = new ArrayList<ContactPojo>();
		Branch branch = findOne(branchId);
		if (branch != null && CollectionUtils.isEmpty(branch.getContacts()) == false) {
			for (Contact contact : branch.getContacts()) {
				list.add(contactService.convertEntityToPojo(contact));
			}
		}
		return list;
	}

	public List<PersonDTO> findUserBranch(Long branchId) {
		List<PersonDTO> persons = new ArrayList<PersonDTO>();
		Branch branch = findOne(branchId);
		if (branch != null && CollectionUtils.isEmpty(branch.getPersons()) == false) {
			for (Person person : branch.getPersons()) {
				persons.add(personService.convertEntityToPojo(person));
			}
		}

		return persons;
	}

	public List<BranchDTO> findBranchsDTOUser(Person person) {
		Set<Branch> branchs = this.getRepository().findByPersons(person);
		Map<Long, BranchDTO> map = new HashMap<Long, BranchDTO>();
		if(branchs == null) {
			branchs = new HashSet<Branch>();
		}
		
		branchs.forEach(index -> {
			BranchDTO dto = convertModelToDto(index);
			dto.setStatus(true);
			map.put(dto.getId(), dto);
		});
		
		List<Branch> allBranchs = this.findAll();
		
		allBranchs.forEach(index -> {
			BranchDTO dto = convertModelToDto(index);
			dto.setStatus(false);
			if(map.containsKey(dto.getId()) == false) {
				map.put(dto.getId(), dto);
			}
		});
		List<BranchDTO> list = new ArrayList<BranchDTO>();
		list.addAll(map.values());
		return list;
	
	}

	public BranchDTO convertModelToDto(Branch index) {
		BranchDTO dto = new BranchDTO();
		dto.setId(index.getId());
		dto.setName(index.getName());
		return dto;
	}

	public Set<Branch> findBranchUser(Person person) {
		return this.getRepository().findByPersons(person);
	}


}
