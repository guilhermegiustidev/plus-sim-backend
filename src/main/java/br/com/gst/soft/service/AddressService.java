package br.com.gst.soft.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.address.Address;
import br.com.gst.soft.entity.branch.BranchAddress;
import br.com.gst.soft.entity.partner.PartnerAddress;
import br.com.gst.soft.pojo.AddressPojo;
import br.com.gst.soft.repository.AddressRepository;
import br.com.gst.soft.rules.AddressDependency;
import br.com.gst.soft.rules.AddressDependencyService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AddressService extends AbstractService<Address, AddressRepository> {

	@Transactional(propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	public <T extends AddressDependency> void mergeAddressess(List<AddressPojo> addressList, AddressDependencyService<T> serviceAddressDependency, Long dependencyId) {
		Map<Long, Address> mapAddress = new HashMap<Long, Address>();
		if(CollectionUtils.isEmpty(addressList) == false) {
			for (AddressPojo index : addressList) {
				Address mergeAddress = null;

				if(index.getId() != null) {
					mergeAddress = this.findOne(index.getId());
				} else {
					mergeAddress = new Address();
				}
				mergeAddress.setId(index.getId());
				mergeAddress.setAddress(index.getAddress());
				mergeAddress.setLat(index.getLat());
				mergeAddress.setLog(index.getLog());
				mergeAddress.setZip(index.getZip());
				mergeAddress.setCountry(index.getCountry());
				mergeAddress.setState(index.getState());
				mergeAddress.setCity(index.getCity());
				mergeAddress.setStatus(index.isStatus());			
				mergeAddress = this.merge(mergeAddress);
				index.setId(mergeAddress.getId());
				mapAddress.put(mergeAddress.getId(), mergeAddress);
			}
		}
		
		if(CollectionUtils.isEmpty(addressList) == false) {
			for (AddressPojo address : addressList) {
				if(mapAddress.containsKey(address.getId())) {
					AddressDependency dependency = serviceAddressDependency.findByAddressAndDependency(address.getId(), dependencyId);
					dependency.setDependency(dependencyId);
					dependency.setAddress(address.getId());
					dependency.setStatus(address.isStatus());
					dependency.setPrincipal(address.isPrimary());
					serviceAddressDependency.mergeDependency((T) dependency);
				}
			}
		}
		
		
	}

	public AddressPojo findPojoAddress(Long addressId) {
		Address addressEntity = this.findOne(addressId);
		AddressPojo pojo = new AddressPojo();
		pojo.setId(addressId);
		pojo.setAddress(addressEntity.getAddress());
		pojo.setLat(addressEntity.getLat());
		pojo.setLog(addressEntity.getLog());
		pojo.setZip(addressEntity.getZip());
		pojo.setCountry(addressEntity.getCountry());
		pojo.setState(addressEntity.getState());
		pojo.setCity(addressEntity.getCity());
		pojo.setStatus(addressEntity.isStatus());
		return pojo;
	}

}
