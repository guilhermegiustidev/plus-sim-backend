package br.com.gst.soft.service;

import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.sim.Lot;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.SimLot;
import br.com.gst.soft.repository.SimLotRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimLotService extends AbstractService<SimLot, SimLotRepository> {

	public Optional<SimLot> findBySimAndLot(Sim sim, Lot lot){
		return this.getRepository().findBySimAndLot(sim, lot);
	}
	
	public Optional<SimLot> findBySim(Sim sim){
		return this.getRepository().findBySim(sim);
	}
	
}
