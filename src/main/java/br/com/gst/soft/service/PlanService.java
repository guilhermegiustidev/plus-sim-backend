package br.com.gst.soft.service;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.servicos.Plan;
import br.com.gst.soft.repository.PlanRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PlanService extends AbstractService<Plan, PlanRepository> {

}
