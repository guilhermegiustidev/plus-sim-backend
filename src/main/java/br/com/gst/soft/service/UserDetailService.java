package br.com.gst.soft.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		try {
			log.info("Loading user = {} ", login);
			Optional<Person> userApplication = userRepository.findByLogin(login);
			Person user = userApplication.orElseThrow(() -> new UsernameNotFoundException("User Error in login or password"));
			log.info("User Loaded = {}", login);
			return new User(login, user.getPassword(), getAuthorities(user));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		throw new UsernameNotFoundException("User Error in login or password");
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Person user) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<SimpleGrantedAuthority>();
		log.info("Loading Authorities = {}", user.getLogin());
		user.getProfiles().forEach(
				profile -> profile.getRoles().forEach(
						role -> authorities.add(
							new SimpleGrantedAuthority(role.getDescription().toUpperCase())
						)
				)
		);
		return authorities;
	}

	
}
