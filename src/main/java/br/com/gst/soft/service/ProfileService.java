package br.com.gst.soft.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.secutiry.Profile;
import br.com.gst.soft.entity.secutiry.Program;
import br.com.gst.soft.entity.secutiry.Role;
import br.com.gst.soft.entity.sim.SimLot;
import br.com.gst.soft.pojo.ProfileDTO;
import br.com.gst.soft.repository.ProfileRepository;
import br.com.gst.soft.repository.ProgramRepository;
import br.com.gst.soft.repository.RoleRepository;
import br.com.gst.soft.repository.UserRepository;
import br.com.gst.soft.response.ProfilePojo;
import br.com.gst.soft.response.RoleDTO;
import br.com.gst.soft.response.UserProfileDTO;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProfileService extends AbstractService<Profile, ProfileRepository> {

	private static String defaultRoles[] = { "ROLE_REGISTER_VERIFY", "ROLE_SIGN_USER", "ROLE_SIGN_FACEBOOK_USER",
			"ROLE_FORGET_ACCOUNT", "ROLE_CHANGE_PASSWORD", };

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProgramService programService;

	public List<RoleDTO> findProfileRoles(Long idProfile) {
		List<RoleDTO> rolesReturn = new ArrayList<RoleDTO>();
		Profile profile = null;
		if (idProfile != null) {
			profile = this.findOne(idProfile);
		}
		List<Role> roles = roleRepository.findRolesNotInDescription(defaultRoles);

		for (Role role : roles) {
			rolesReturn.add(RoleDTO.builder().id(role.getId()).idProfile(idProfile).roleName(role.getDescription())
					.status(profile != null ? profile.getRoles().contains(role) : false).build());
		}
		return rolesReturn;
	}

	public List<UserProfileDTO> findAllUsers(Long idProfile) {
		List<UserProfileDTO> list = new ArrayList<UserProfileDTO>();
		if (idProfile != null) {
			Profile profile = this.findOne(idProfile);
			if (profile != null) {
				List<Person> persons = profile.getUsers();
				if (persons != null) {
					for (Person person : persons) {
						list.add(UserProfileDTO.builder().idProfile(idProfile).idUser(person.getId()).status(true)
								.userName(person.getName()).build());
					}
				}
			}
		}
		return list;
	}

	private List<Long> extractIdsRoles(final ProfilePojo entity, Predicate<RoleDTO> predicate,
			Function<RoleDTO, Long> function) {
		return entity.getRoles().stream().filter(predicate).collect(Collectors.toList()).stream().map(function)
				.collect(Collectors.toList());
	}

	private List<Long> extractIdsUsers(final ProfilePojo entity, Predicate<UserProfileDTO> predicate,
			Function<UserProfileDTO, Long> function) {
		return entity.getUsers().stream().filter(predicate).collect(Collectors.toList()).stream().map(function)
				.collect(Collectors.toList());
	}

	public Profile mergeProfile(ProfilePojo entity) {
		Profile profile = entity.getIdProfile() != null ? this.findOne(entity.getIdProfile()) : new Profile();
		profile.setDescription(entity.getNameProfile());
		profile.setStatus(entity.isStatus());
		profile = this.merge(profile);

		Function<RoleDTO, Long> functionGetId = RoleDTO::getId;
		removeRoles(entity, functionGetId, profile);
		addRoles(entity, functionGetId, profile);

		Function<UserProfileDTO, Long> funcionGetIdUser = UserProfileDTO::getIdUser;
		removeUsers(entity, profile, funcionGetIdUser);
		addUsers(entity, profile, funcionGetIdUser);
		profile = this.merge(profile);
		programService.clearProgramas(entity.getModules(), profile);
		return profile;
	}

	private void addUsers(ProfilePojo entity, Profile profile, Function<UserProfileDTO, Long> funcionGetIdUser) {
		List<Long> activeUsers = extractIdsUsers(entity, index -> index.isStatus(), funcionGetIdUser);
		for (Long id : activeUsers) {
			if (profile.getUsers() == null) {
				profile.setUsers(new ArrayList<Person>());
			}
			Optional<Person> optionalPerson = this.userRepository.findById(id);
			if (optionalPerson.isPresent() && profile.getUsers().contains(optionalPerson.get()) == false) {
				profile.getUsers().add(optionalPerson.get());
			}
		}
	}

	private void addRoles(ProfilePojo entity, Function<RoleDTO, Long> function, Profile profile) {
		List<Long> activeRoles = extractIdsRoles(entity, index -> index.isStatus(), function);
		for (Long id : activeRoles) {
			if (profile.getRoles() == null) {
				profile.setRoles(new ArrayList<Role>());
			}
			Optional<Role> optionalRole = this.roleRepository.findById(id);
			if (optionalRole.isPresent() && profile.getRoles().contains(optionalRole.get()) == false) {
				profile.getRoles().add(optionalRole.get());
			}
		}
	}

	private void removeUsers(ProfilePojo entity, Profile profile, Function<UserProfileDTO, Long> funcionGetIdUser) {
		List<Long> inactiveUsers = extractIdsUsers(entity, index -> !index.isStatus(), funcionGetIdUser);
		if (profile.getUsers() != null) {
			for (Iterator<Person> iterator = profile.getUsers().iterator(); iterator.hasNext();) {
				if (inactiveUsers.contains(iterator.next().getId())) {
					iterator.remove();
				}
			}
		}
	}

	private void removeRoles(ProfilePojo entity, Function<RoleDTO, Long> function, Profile profile) {
		List<Long> inactiveRoles = extractIdsRoles(entity, index -> !index.isStatus(), function);
		if (profile.getRoles() != null) {
			for (Iterator<Role> iterator = profile.getRoles().iterator(); iterator.hasNext();) {
				if (inactiveRoles.contains(iterator.next().getId())) {
					iterator.remove();
				}
			}
		}
	}

	public Set<Profile> findByLikeName(String name) {
		return this.getRepository().findByLikeName(name);
	}

	public List<ProfileDTO> findProfilesUser(Person person) {
		Map<Long, ProfileDTO> map = new HashMap<Long, ProfileDTO>();
		Set<Profile> branchs = person.getProfiles();
		if(branchs == null) {
			branchs = new HashSet<Profile>();
		}
		
		branchs.forEach(index -> {
			ProfileDTO dto = convertModelToDto(index);
			dto.setStatus(true);
			map.put(dto.getId(), dto);
		});
		
		List<Profile> allBranchs = this.findAll();
		
		allBranchs.forEach(index -> {
			ProfileDTO dto = convertModelToDto(index);
			dto.setStatus(false);
			if(map.containsKey(dto.getId()) == false) {
				map.put(dto.getId(), dto);
			}
		});
		List<ProfileDTO> list = new ArrayList<ProfileDTO>();
		list.addAll(map.values());
		return list;
	
	}

	private ProfileDTO convertModelToDto(Profile index) {
		ProfileDTO dto = new ProfileDTO();
		dto.setId(index.getId());
		dto.setDescription(index.getDescription());
		return dto;
	}

	@Transactional(propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	public void mergeProfilesUser(Person merge, List<ProfileDTO> profiles) {
		Map<Long, Profile> mapProfile = new HashMap<Long, Profile>();
		if(CollectionUtils.isEmpty(profiles) == false) {
			for (ProfileDTO index : profiles) {
				Long id = index.getId();
				mapProfile.put(id, this.findOne(id));
			}
			if(merge.getProfiles() == null) {
				merge.setProfiles(new HashSet<Profile>());
			}
			for (ProfileDTO index : profiles) {
				Long id = index.getId();
				if(mapProfile.containsKey(id)) {
					Profile profile = mapProfile.get(id);
					if(index.isStatus()) {
						merge.getProfiles().add(profile);
					} else {
						merge.getProfiles().remove(profile);
					}
				}
			}
		}
	}

	public Set<String> findAllPrograms(Long idProfile) {
		Profile profile = this.findOne(idProfile);
		Example<Program> example = Example.of(Program.builder().profile(profile).build());
		List<Program> allProgramas = this.programService.findAll(example);
		return allProgramas.stream().collect(Collectors.toList()).stream().map(Program::getName).collect(Collectors.toSet());
	}

}
