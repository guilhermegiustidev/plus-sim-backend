package br.com.gst.soft.service.validation.program;

import br.com.gst.soft.response.ProgramValidationResponse;

public interface ValidationProgram {

	public ProgramValidationResponse validate();
	
}
