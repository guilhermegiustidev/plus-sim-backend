package br.com.gst.soft.service;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.localization.State;
import br.com.gst.soft.repository.StateRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StateService extends AbstractService<State, StateRepository> {

	public List<State> findByCountryId(Long id){
		return this.getRepository().findByCountryId(id);
	}

	
}
