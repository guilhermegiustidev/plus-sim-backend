package br.com.gst.soft.service;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.localization.Country;
import br.com.gst.soft.repository.CountryRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CountryService extends AbstractService<Country, CountryRepository> {

	
}
