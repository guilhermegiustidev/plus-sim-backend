package br.com.gst.soft.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.localization.City;
import br.com.gst.soft.repository.CityRepository;
import br.com.gst.soft.repository.mapper.LocationJdbcRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CityService extends AbstractService<City, CityRepository> {

	public List<City> findByStateId(Long id) {
		return this.getRepository().findByStateId(id);
	}

}
