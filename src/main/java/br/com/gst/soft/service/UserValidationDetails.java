package br.com.gst.soft.service;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.com.gst.soft.entity.secutiry.Person;

public interface UserValidationDetails {

	public void validateUser(Person user) throws UsernameNotFoundException;
	
}
