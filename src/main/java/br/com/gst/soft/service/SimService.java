package br.com.gst.soft.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.sim.Lot;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.Sim.SimChipStatus;
import br.com.gst.soft.entity.sim.SimLot;
import br.com.gst.soft.repository.BranchRepository;
import br.com.gst.soft.repository.SimLotRepository;
import br.com.gst.soft.repository.SimRepository;
import br.com.gst.soft.response.SimInvoiceResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimService extends AbstractService<Sim, SimRepository> {

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private SimLotRepository simLotRepository;

	public List<Sim> findByIds(Set<Long> ids){
		return this.getRepository().findAllById(ids);
	}
	
	public Optional<Sim> findByCode(final String code) {
		return getRepository().findByCode(code);
	}

	public boolean verifyBeforeExclude(Sim entity) {
		log.warn("VERIFICAR ANTES DE EXCLUIR");
		return false;
	}

	public SimInvoiceResponse findSimAvailable(String code, Long branchId) {
		Optional<Sim> optionalSim = this.findByCode(code);
		Optional<Branch> branchOptional = branchRepository.findById(branchId);
		if(optionalSim.isPresent() == false) {
			return SimInvoiceResponse.builder().available(false).title("MANAGE_REQUEST_SIM.ERROR.NOT_EXISTS.TITLE")
					.message("MANAGE_REQUEST_SIM.ERROR.NOT_EXISTS.BODY").build();
		}
		if (optionalSim.isPresent() && branchOptional.isPresent()) {
			Branch branch = branchOptional.get();
			Sim sim = optionalSim.get();

			if (SimChipStatus.FREE.equals(sim.getStatus()) == false) {
				return SimInvoiceResponse.builder().available(false).title("MANAGE_REQUEST_SIM.ERROR.STATUS.TITLE")
						.message("MANAGE_REQUEST_SIM.ERROR.STATUS.BODY").build();
			}

			Optional<SimLot> simLotOptional = simLotRepository.findBySim(sim);
			if (simLotOptional.isPresent()) {
				SimLot simLot = simLotOptional.get();
				Lot lot = simLot.getLot();
				if (lot.getBranch().equals(branch) == false) {
					return SimInvoiceResponse.builder().available(false)
							.title("MANAGE_REQUEST_SIM.ERROR.OTHER_LOT.TITLE")
							.message("MANAGE_REQUEST_SIM.ERROR.OTHER_LOT.BODY").build();
				}
				return SimInvoiceResponse.builder().simId(sim.getId()).lotId(lot.getId()).code(code)
						.providerId(sim.getProvider().getId()).available(true).status(sim.getStatus()).build();
			}

		}
		return SimInvoiceResponse.builder().available(false).title("MANAGE_REQUEST_SIM.ERROR.INVALID.TITLE")
				.message("MANAGE_REQUEST_SIM.ERROR.INVALID.BODY").build();
	}

}