package br.com.gst.soft.service.menu;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.menu.BadgeMenu;
import br.com.gst.soft.menu.MenuProcessor;

@Service("MENU_ORD03")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ManagerPartnerOrdersRequestsMenu implements MenuProcessor {

	@Override
	public BadgeMenu process() {
		return BadgeMenu.empty();
	}

}
