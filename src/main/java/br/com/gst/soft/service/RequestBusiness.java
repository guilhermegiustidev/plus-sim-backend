package br.com.gst.soft.service;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.servicos.Pabx;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestBusiness {

	public boolean verifyStatusPabx (final Pabx pabx) {
		log.warn("Implementar Futuramente");
		return pabx.getId() % 2 == 0;
	}
	
}
