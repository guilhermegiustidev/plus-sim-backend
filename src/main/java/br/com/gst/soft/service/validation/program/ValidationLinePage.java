package br.com.gst.soft.service.validation.program;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.servicos.PlanProvider;
import br.com.gst.soft.response.ProgramValidationResponse;
import br.com.gst.soft.service.PlanProviderService;
import br.com.gst.soft.service.ProviderService;

@Service("VALIDATION_ADM03")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValidationLinePage implements ValidationProgram{

	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private PlanProviderService planProviderService;
	
	@Override
	public ProgramValidationResponse validate() {
		
		ProgramValidationResponse response = new ProgramValidationResponse();
		response.setCanNavigate(true);
		
		ExampleMatcher matcher = ExampleMatcher.matchingAny();
		Provider provider = new Provider();
		provider.setStatus(true);
		Example<Provider> exampleProvider = Example.of(provider, matcher);
		List<Provider> allActiveProvider = providerService.findAll(exampleProvider);
		
		PlanProvider planProvider = new PlanProvider();
		provider.setStatus(true);
		Example<PlanProvider> examplePlanProvider = Example.of(planProvider, matcher);
		List<PlanProvider> allActivePlanProvider = planProviderService.findAll(examplePlanProvider);
		
		if(CollectionUtils.isEmpty(allActiveProvider) || CollectionUtils.isEmpty(allActivePlanProvider)) {
			response.setCanNavigate(false);
			response.setLabels("LINE.ERROR.DEPENDENCIES");
		}
		
		return response;
	}

}
