package br.com.gst.soft.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.pojo.ContactPojo;
import br.com.gst.soft.repository.ContactRepository;
import br.com.gst.soft.rules.HasContacts;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ContactService extends AbstractService<Contact, ContactRepository> {

	@Transactional(propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	public void mergeContacts(HasContacts merge, List<ContactPojo> contactsPojoList) {
		
		Map<Long, Contact> mapContacts = new HashMap<Long, Contact>();
		if(CollectionUtils.isEmpty(contactsPojoList) == false) {
			for (ContactPojo index : contactsPojoList) {
				Contact mergeContact = null;

				if(index.getId() != null) {
					mergeContact = this.findOne(index.getId());
				} else {
					mergeContact = new Contact();
				}
				mergeContact.setId(index.getId());
				mergeContact.setName(index.getName());
				mergeContact.setStatus(index.isStatus());
				mergeContact.setType(index.getType());
				switch (index.getType()) {
					case EMAIL:
						mergeContact.setValue(index.getEmail());
						break;
					case PHONE:
						mergeContact.setValue(index.getPhone());
						break;
				}
				mergeContact = super.merge(mergeContact);
				index.setId(mergeContact.getId());
				mapContacts.put(mergeContact.getId(), mergeContact);
			}
		}
		
		
		if(CollectionUtils.isEmpty(contactsPojoList) == false) {
			if(merge.getContacts()  == null) {
				merge.setContacts(new HashSet<Contact>());
			}
			for (ContactPojo index : contactsPojoList) {
				if(mapContacts.containsKey(index.getId())) {
					if(!index.isStatus()) {
						merge.getContacts().remove(mapContacts.get(index.getId()));
					} else {
						merge.getContacts().add(mapContacts.get(index.getId()));
					}
				}
			}
		}
	}

	public ContactPojo convertEntityToPojo(Contact contact) {
		ContactPojo pojo = new ContactPojo();
		pojo.setId(contact.getId());
		pojo.setName(contact.getName());
		switch (contact.getType()) {
		case EMAIL:
			pojo.setEmail(contact.getValue());
			break;
		case PHONE:
			pojo.setPhone(contact.getValue());
			break;
		}
		pojo.setType(contact.getType());
		pojo.setStatus(contact.isStatus());
		return pojo;
	}

	
}
