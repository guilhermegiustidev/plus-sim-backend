package br.com.gst.soft.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.servicos.PlanProvider;
import br.com.gst.soft.repository.PlanProviderRepository;
import br.com.gst.soft.repository.ProviderRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PlanProviderService extends AbstractService<PlanProvider, PlanProviderRepository> {

	@Autowired
	private ProviderRepository providerRepository;
	
	public Set<PlanProvider> findPlanProviderByProviderId(Long id) {
		Optional<Provider> provider = providerRepository.findById(id);
		return this.getRepository().findByProvider(provider.orElseThrow(() -> new RuntimeException("Erro ao buscar o fornecedor")));
	}

}
