package br.com.gst.soft.service.validation.program;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.servicos.PlanProvider;
import br.com.gst.soft.response.ProgramValidationResponse;
import br.com.gst.soft.security.UserDataSession;
import br.com.gst.soft.service.BranchService;
import br.com.gst.soft.service.ProviderService;

@Service("VALIDATION_ADM12")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValidationSimChipPage implements ValidationProgram{

	@Autowired
	private BranchService branchService;
	
	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private UserDataSession userSession;
	
	@Override
	public ProgramValidationResponse validate() {
		
		ProgramValidationResponse response = new ProgramValidationResponse();
		response.setCanNavigate(true);
		
		ExampleMatcher matcher = ExampleMatcher.matchingAny();
		Provider provider = new Provider();
		provider.setStatus(true);
		Example<Provider> exampleProvider = Example.of(provider, matcher);
		List<Provider> allActiveProvider = providerService.findAll(exampleProvider);
		
		Branch branch = new Branch();
		branch.setStatus(true);
		Example<Branch> branchExample = Example.of(branch, matcher);
		List<Branch> allActiveBranchs = branchService.findAll(branchExample);
		
		if(response.isCanNavigate() && CollectionUtils.isEmpty(allActiveProvider) || CollectionUtils.isEmpty(allActiveBranchs)) {
			response.setCanNavigate(false);
			response.setLabels("LOT.ERROR.DEPENDENCIES");
		}
		
		if(response.isCanNavigate() && CollectionUtils.isEmpty(branchService.findBranchUser(userSession.getUserLogged()))) {
			response.setCanNavigate(false);
			response.setLabels("LOT.ERROR.NOT_BRANCHS_AVAILABLE");
		}

		
		return response;
	}

}
