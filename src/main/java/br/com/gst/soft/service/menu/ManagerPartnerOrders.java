package br.com.gst.soft.service.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.menu.BadgeMenu;
import br.com.gst.soft.menu.MenuProcessor;

@Service("MENU_ORDER_PARTNER")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ManagerPartnerOrders implements MenuProcessor{

	@Autowired
	private ManagerPartnerOrdersChipMenu partnerOrdersChipMenu;
	
	@Autowired
	private ManagerPartnerOrdersRequestsMenu partnerOrdersRequestsMenu;
	
	@Override
	public BadgeMenu process() {
		return partnerOrdersChipMenu.process().add(this.partnerOrdersRequestsMenu.process());
	}

}
