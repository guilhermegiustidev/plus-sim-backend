package br.com.gst.soft.service;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.address.Address;
import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.entity.request.invoice.InvoiceRequestChip;
import br.com.gst.soft.entity.request.invoice.InvoiceRequestChipItem;
import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.entity.request.sim.RequestStatus;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.Sim.SimChipStatus;
import br.com.gst.soft.pojo.InvoiceSimDTO;
import br.com.gst.soft.repository.AddressRepository;
import br.com.gst.soft.repository.BranchRepository;
import br.com.gst.soft.repository.InvoiceRequestChipRepository;
import br.com.gst.soft.repository.InvoiceRequestItemChipRepository;
import br.com.gst.soft.repository.PartnerRepository;
import br.com.gst.soft.repository.RequestSimChipRepository;
import br.com.gst.soft.repository.SimRepository;
import br.com.gst.soft.security.UserDataSession;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class InvoiceRequestChipService extends AbstractService<InvoiceRequestChip, InvoiceRequestChipRepository> {

	@Autowired
	private SimRepository simRepository;

	@Autowired
	private PartnerRepository partnerRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private RequestSimChipRepository requestSimChipRepository;

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private UserDataSession userDataSession;

	@Autowired
	private InvoiceRequestItemChipRepository invoiceRequestItemChipRepository;
	
	public InvoiceRequestChip proccessRequest(InvoiceSimDTO dto) {
		if (dto.getId() == null) {
			RequestSimChip requestSimChip = this.requestSimChipRepository.getOne(dto.getRequestId());
			Branch branch = this.branchRepository.getOne(dto.getBranchId());
			Partner partner = this.partnerRepository.getOne(dto.getPartnerId());
			Address address = this.addressRepository.getOne(dto.getAddressId());
			List<Sim> sims = this.simRepository.findAllById(dto.getSims());

			List<InvoiceRequestChipItem> items = new ArrayList<InvoiceRequestChipItem>(); 
			
			for (Sim sim : sims) {
				if (SimChipStatus.FREE.equals(sim.getStatus()) == false) {
					throw new RuntimeException("Chip Indisponivel");
				}
				sim.setStatus(SimChipStatus.RESERVED_INVOICE);
				items.add(InvoiceRequestChipItem.builder().sim(sim).build());
			}

			InvoiceRequestChip invoiceRequestChip = this.merge(InvoiceRequestChip.builder().bornDate(DateTime.now().toDate()).address(address)
					.person(userDataSession.getUserLogged()).partner(partner).request(requestSimChip).branch(branch)
					.build());
			
			items.forEach(index -> index.setInvoice(invoiceRequestChip));
			invoiceRequestItemChipRepository.saveAll(items);
			simRepository.saveAll(sims);
			requestSimChip.setStatus(RequestStatus.WAITING_DISPATCH);
			this.requestSimChipRepository.save(requestSimChip);
			return invoiceRequestChip;
		}
		throw new RuntimeException("Chip Indisponivel");
	}

}
