package br.com.gst.soft.service;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.repository.ProviderRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProviderService extends AbstractService<Provider, ProviderRepository> {

}