package br.com.gst.soft.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.secutiry.Menu;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.secutiry.Profile;
import br.com.gst.soft.entity.secutiry.Program;
import br.com.gst.soft.repository.ProgramRepository;
import br.com.gst.soft.response.MenuResponse;
import br.com.gst.soft.response.ModuleProfileDTO;
import br.com.gst.soft.security.UserDataSession;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProgramService extends AbstractService<Program, ProgramRepository> {

	@Autowired
	private UserDataSession userDataSession;
	
	public void clearProgramas(Set<ModuleProfileDTO> modules, Profile profile) {
		List<Program> programas = this.getRepository().findByProfile(profile);
		this.getRepository().deleteAll(programas);

		if(CollectionUtils.isEmpty(modules) == false) {
			for (ModuleProfileDTO program : modules) {
				Menu menu = Menu.valueOf(program.getData());
				mergeProgram(profile, menu);
			}
		}
	}

	private void mergeProgram(Profile profile, Menu menu) {
		if(this.getRepository().findByNameAndProfile(menu.toString(), profile).isPresent() == false) {
			this.getRepository().save(Program.builder().name(menu.toString()).profile(profile).build());
		}
		if(menu.getParent() != null) {
			this.mergeProgram(profile, menu.getParent());
		}
	}

	public boolean verifyPermission(String menuId) {
		Person userLogged = this.userDataSession.getUserLogged();
		Set<Profile> profiles = userLogged.getProfiles();
		
		for (Profile profile : profiles) {
			Example<Program> example = Example.of(Program.builder().name(menuId).profile(profile).build());
			List<Program> programs = this.findAll(example);
			if(programs.isEmpty() == false) {
				return true;
			}
		}
		return false;
	}

	public List<MenuResponse> findMenus() {
		return buildMenus(new ArrayList<MenuResponse>(), Menu.findByParent(null));
	}

	private List<MenuResponse> buildMenus(List<MenuResponse> response, List<Menu> menus) {
		for (Menu menu : menus) {
			MenuResponse menuResponse = MenuResponse.builder().data(menu.toString()).label("MODULE.PROGRAM." + menu.toString()).build();
			List<Menu> children = menu.findChildren();
			if(children.isEmpty() == false) {
				menuResponse.setChildren(new ArrayList<MenuResponse>());
			}
			response.add(menuResponse);
			this.buildMenus(menuResponse.getChildren(), children);
		}
		return response;
	}

}
