package br.com.gst.soft.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.address.Address;
import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.entity.request.invoice.InvoiceRequestChip;
import br.com.gst.soft.entity.request.sim.RequestItemSimChip;
import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.Sim.SimChipStatus;
import br.com.gst.soft.pojo.InvoiceSimDTO;
import br.com.gst.soft.repository.AddressRepository;
import br.com.gst.soft.repository.BranchRepository;
import br.com.gst.soft.repository.PartnerRepository;
import br.com.gst.soft.repository.RequestItemSimChipRepository;
import br.com.gst.soft.repository.RequestSimChipRepository;
import br.com.gst.soft.repository.SimRepository;
import br.com.gst.soft.repository.mapper.SimRepositoryJdbc;
import br.com.gst.soft.response.invoice.sim.ValidationInvoiceChipResponse;
import br.com.gst.soft.security.UserDataSession;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestSimChipService extends AbstractService<RequestSimChip, RequestSimChipRepository> {

	@Autowired
	private RequestItemSimChipRepository requestItemSimChipRepository; 
	
	@Autowired
	private BranchRepository branchRepository;
	
	@Autowired
	private SimRepositoryJdbc simRepositoryJdbc;
	
	@Override
	public RequestSimChip merge(final RequestSimChip entity) {
		List<RequestItemSimChip> items = entity.getItems();
		RequestSimChip merge = super.merge(entity);
		items.forEach(index -> index.setRequestSim(merge));
		List<RequestItemSimChip> itemsMerged = requestItemSimChipRepository.saveAll(items);
		merge.setItems(itemsMerged);
		return merge;
	}
	
	public void buildDataPage(Page<RequestSimChip> page){
		if(page.hasContent()) {
			for (RequestSimChip requestSimChip : page) {
				Integer quantity = 0;
				BigDecimal price = BigDecimal.ZERO;
				for (RequestItemSimChip item : requestSimChip.getItems()) {
					quantity += item.getQuantity();
					price = price.add(item.getProvider().getValorChipVenda()).multiply(BigDecimal.valueOf(quantity.longValue()));
				}
				requestSimChip.setPrice(price);
				requestSimChip.setQuantity(quantity);
			}
		}
	}

	public ValidationInvoiceChipResponse validateRequestSimItems(Long requestId, Long branchId) {
		RequestSimChip requestSimChip = this.findOne(requestId);
		Optional<Branch> branchOption = branchRepository.findById(branchId);
		ValidationInvoiceChipResponse response = new ValidationInvoiceChipResponse();
		if(branchOption.isPresent()) {
			if(requestSimChip != null) {
				if(requestSimChip.getItems() != null) {
					for (RequestItemSimChip item : requestSimChip.getItems()) {	
						Long quantityInStock = simRepositoryJdbc.countChipsByProvider(branchId, item.getProvider().getId());
						response.addItem(item.getProvider().getId(), item.getQuantity(), quantityInStock, item.getProvider().getValorChipVenda());
					}
				}
			}
		}
		return response.procced();
	}
	
}
