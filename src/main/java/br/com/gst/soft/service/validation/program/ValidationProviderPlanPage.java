package br.com.gst.soft.service.validation.program;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.response.ProgramValidationResponse;
import br.com.gst.soft.service.ProviderService;

@Service("VALIDATION_ADM09")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValidationProviderPlanPage implements ValidationProgram{

	@Autowired
	private ProviderService service;
	
	@Override
	public ProgramValidationResponse validate() {
		
		ProgramValidationResponse response = new ProgramValidationResponse();
		response.setCanNavigate(true);
		
		ExampleMatcher matcher = ExampleMatcher.matchingAny();
		Provider entity = new Provider();
		
		entity.setStatus(true);
		Example<Provider> exampleProvider = Example.of(entity, matcher);
		List<Provider> allActiveProvider = service.findAll(exampleProvider);
		
		if(CollectionUtils.isEmpty(allActiveProvider)) {
			response.setCanNavigate(false);
			response.setLabels("PROVIDER_PLAN.ERROR.DEPENDENCIES");
		}
		
		return response;
	}

}
