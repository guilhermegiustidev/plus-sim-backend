package br.com.gst.soft.service;

import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.servicos.PlanServiceType;
import br.com.gst.soft.repository.PlanServiceTypeRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PlanServiceTypeService extends AbstractService<PlanServiceType, PlanServiceTypeRepository> {

	public Set<PlanServiceType> findByLikeName(String name) {
		return getRepository().findByLikeName(name);
	}

}
