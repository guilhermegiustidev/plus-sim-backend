package br.com.gst.soft.token;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.catalina.util.ParameterMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.secutiry.RefreshToken;
import br.com.gst.soft.entity.secutiry.Token;
import br.com.gst.soft.repository.TokenRepository;
import br.com.gst.soft.service.PersonService;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RefreshTokenPreProcessorFilter implements Filter{
	
	@Autowired
	private TokenRepository tokenRepository;
	
	@Autowired
	private PersonService userService;
	
	@Override public void init(FilterConfig filterConfig) throws ServletException {}
	@Override public void destroy() {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		
		boolean uriCompare = "/oauth/token".equalsIgnoreCase(httpServletRequest.getRequestURI());
		boolean refreshTokenParameter = "refresh_token".equals(httpServletRequest.getParameter("grant_type"));
		
		if(uriCompare && refreshTokenParameter) {
			String username = httpServletRequest.getParameter("username");
			String tokenhash = httpServletRequest.getParameter("access_token");

			if(StringUtils.isEmpty(username) == false && StringUtils.isEmpty(tokenhash) == false) {
				Optional<Person> user = userService.findUserByLogin(username);
				if(user.isPresent()) {
					Optional<Token> optionalToken = tokenRepository.findByTokenAndUser(tokenhash, user.get());
					if(optionalToken.isPresent()) {
						Token token = optionalToken.get();
						RefreshToken refreshToken = token.getRefreshToken();
						httpServletRequest = new GstServletRequestWrapper(httpServletRequest, refreshToken.getToken(), refreshToken.getId());
					}
					
				}
			}
		}
		chain.doFilter(httpServletRequest , response);
	}

	static class GstServletRequestWrapper extends HttpServletRequestWrapper {

		private String refreshToken;
		private Long refreshTokenId;
		
		public GstServletRequestWrapper(HttpServletRequest request, String refreshToken, Long refreshTokenId) {
			super(request);
			this.refreshToken = refreshToken;
			this.refreshTokenId = refreshTokenId;
		}
	
		@Override
		public String getParameter(String name){
			switch (name) {
			case "refresh_token_id":
				if(refreshTokenId != null) {
					return this.refreshTokenId.toString();
				}
				break;
			case "refresh_token":
				return refreshToken;
			}
			return super.getParameter(name);
		}
		
		@Override
		public Map<String, String[]> getParameterMap() {
			ParameterMap<String, String[]> map = new ParameterMap<>(getRequest().getParameterMap());
			map.put("refresh_token", new String[] {this.refreshToken});
			map.put("refresh_token_id", new String[] {this.refreshTokenId.toString()});
			map.setLocked(true);
			return map;
		}
		
	}
	

}
