package br.com.gst.soft.token;

import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.secutiry.RefreshToken;
import br.com.gst.soft.entity.secutiry.Token;
import br.com.gst.soft.repository.RefreshTokenRepository;
import br.com.gst.soft.repository.TokenRepository;
import br.com.gst.soft.service.PersonService;

@ControllerAdvice
public class RefreshTokenPostProcessor implements ResponseBodyAdvice<OAuth2AccessToken> {

	@Autowired
	private PersonService service;

	@Autowired
	private RefreshTokenRepository refreshTokenRepository;

	@Autowired
	private TokenRepository tokenRepository;

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return returnType.getMethod().getName().equals("postAccessToken");
	}

	@Override
	public OAuth2AccessToken beforeBodyWrite(OAuth2AccessToken body, MethodParameter returnType,
			MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType,
			ServerHttpRequest request, ServerHttpResponse response) {

		String refreshToken = body.getRefreshToken().getValue();
		HttpServletRequest req = ((ServletServerHttpRequest) request).getServletRequest();
		HttpServletResponse res = ((ServletServerHttpResponse) response).getServletResponse();

		saveRefreshToken(req, body);

		addRefreshTokenOnCookier(refreshToken, req, res);
		removeRefreshTokenBody((DefaultOAuth2AccessToken) body);
		return body;
	}

	private void saveRefreshToken(HttpServletRequest req, OAuth2AccessToken body) {
		String username = req.getParameter("username");
		String refreshTokenId = req.getParameter("refresh_token_id");

		Optional<Person> user = service.findUserByLogin(username);
		if (user.isPresent() && !"register@welcome.system".equals(username)) {
			DateTime now = DateTime.now();
			RefreshToken refreshToken = createRefreshToken(user.get(), body, now, refreshTokenId);

			createToken(user.get(), body, refreshToken, now);
		}
	}

	private Token createToken(Person userApplication, OAuth2AccessToken body, RefreshToken refreshToken, DateTime dataCreation) {
		Token token = Token.builder()
			.token(body.getValue())
			.user(userApplication)
			.refreshToken(refreshToken)
			.alive(true)
			.create(dataCreation.toDate())
		.build();
		
		return tokenRepository.save(token);
		
	}

	private RefreshToken createRefreshToken(Person userApplication, OAuth2AccessToken body, DateTime now,
			String refreshTokenId) {
		OAuth2RefreshToken token = body.getRefreshToken();
		RefreshToken refreshToken = refreshTokenId != null
				? refreshTokenRepository.getOne(Long.valueOf(refreshTokenId))
				: null;
		if (refreshToken == null) {
			refreshToken = RefreshToken.builder().user(userApplication).token(token.getValue()).build();
			refreshToken.setCreate(now.toDate());
			refreshToken.setAlive(true);
			refreshToken = refreshTokenRepository.save(refreshToken);
		}
		return refreshToken;

	}

	private void removeRefreshTokenBody(DefaultOAuth2AccessToken token) {
		token.setRefreshToken(null);
	}

	private void addRefreshTokenOnCookier(String refreshToken, HttpServletRequest req, HttpServletResponse res) {
		Cookie refreshTokenCookie = new Cookie("refreshToken", refreshToken);
		refreshTokenCookie.setHttpOnly(true);
		refreshTokenCookie.setSecure(true);
		refreshTokenCookie.setPath(req.getContextPath() + "/oauth/token");
		refreshTokenCookie.setMaxAge(2592000);
		res.addCookie(refreshTokenCookie);
	}

}
