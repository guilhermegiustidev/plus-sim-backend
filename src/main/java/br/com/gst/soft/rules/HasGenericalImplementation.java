package br.com.gst.soft.rules;

public interface HasGenericalImplementation {

	public <T> Class<T> getClazz();
	
}
