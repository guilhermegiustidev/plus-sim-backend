package br.com.gst.soft.rules;

public interface LogicInstance {

	public boolean isStatus();
	public void setStatus(boolean status);
	public void exclude();
	
}
