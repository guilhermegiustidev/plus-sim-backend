package br.com.gst.soft.rules;

import br.com.gst.soft.exception.PojoValidationException;

public interface IPojoParameter {

	public void validate() throws PojoValidationException;
	
}
