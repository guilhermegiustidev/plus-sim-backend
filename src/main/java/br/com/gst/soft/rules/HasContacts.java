package br.com.gst.soft.rules;

import java.util.Set;

import br.com.gst.soft.entity.contact.Contact;

public interface HasContacts {

	public void setContacts(Set<Contact> contacts);
	public Set<Contact> getContacts();
	
}
