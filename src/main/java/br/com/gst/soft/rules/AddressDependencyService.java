package br.com.gst.soft.rules;

public interface AddressDependencyService<T extends AddressDependency> {

	public T findByAddressAndDependency(final Long addressId, final Long dependencyId);
	
	public T mergeDependency(T entity);
}
