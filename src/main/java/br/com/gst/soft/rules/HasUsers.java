package br.com.gst.soft.rules;

import java.util.Set;

import br.com.gst.soft.entity.secutiry.Person;

public interface HasUsers {

	public void setPersons(Set<Person> persons);
	public Set<Person> getPersons();
	
}
