package br.com.gst.soft.rules;

public interface AddressDependency {

	public Long getId();
	public Long getDependency();
	public Long getAddress();
	public boolean isPrincipal();
	public boolean isStatus();
	
	public void setId(Long id);
	public void setDependency(Long dependency);
	public void setAddress(Long address);
	public void setPrincipal(boolean principal);
	public void setStatus(boolean status);
	
	
	
	
}
