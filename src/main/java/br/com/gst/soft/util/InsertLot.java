package br.com.gst.soft.util;

import java.util.ArrayList;
import java.util.List;

public class InsertLot {

	
	public static void main(String[] args) {
		List<String> sims = new ArrayList<String>();
		Long value = 0L;
		System.out.println("INSERT INTO SIN_LOT (CD_CODE, STATUS, ID_PERSON, ID_BRANCH) VALUES ('20180722U1', 'DONE', 1, 1);");
		while(value < 101) {
			String index = "INSERT INTO SIM_CHIP (CD_CODE, ID_PROVIDER, REGEX, ST_STATUS) VALUES "
					+ "('8901260963189522" + String.format("%03d", value) + "F', 1, '^(89012)([0-9]{14})([fF])$', 'TEST1');" ;
			sims.add(index);
			value++;
			System.out.println(index);
		}
		System.out.println("INSERT INTO PLUS_CHIP_SIM_LOT (ID_SIM, ID_LOT, DT_TIME_INCLUDE)\n" + 
				"SELECT CHIP.ID_SIN, LOT.ID_LOT, SYSDATE()\n" + 
				"  FROM SIN_LOT LOT \n" + 
				"  JOIN SIM_CHIP CHIP\n" + 
				" WHERE LOT.CD_CODE = '20180722U1'\n" + 
				"   AND CHIP.ST_STATUS = 'TEST1';");

		System.out.println("INSERT INTO SIN_LOT (CD_CODE, STATUS, ID_PERSON, ID_BRANCH) VALUES ('20180722U2', 'DONE', 2, 1);");
		while(value < 201) {
			String index = "INSERT INTO SIM_CHIP (CD_CODE, ID_PROVIDER, REGEX, ST_STATUS) VALUES "
					+ "('8901260963189522" + String.format("%03d", value) + "F', 1, '^(89012)([0-9]{14})([fF])$', 'TEST2');" ;
			sims.add(index);
			value++;
			System.out.println(index);
		}
		System.out.println("INSERT INTO PLUS_CHIP_SIM_LOT (ID_SIM, ID_LOT, DT_TIME_INCLUDE)\n" + 
				"SELECT CHIP.ID_SIN, LOT.ID_LOT, SYSDATE()\n" + 
				"  FROM SIN_LOT LOT \n" + 
				"  JOIN SIM_CHIP CHIP\n" + 
				" WHERE LOT.CD_CODE = '20180722U2'\n" + 
				"   AND CHIP.ST_STATUS = 'TEST2';");		
		
		
	}
	
}
