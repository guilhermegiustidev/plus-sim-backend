package br.com.gst.soft.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class GenerateNewUserApplication {

	public static void main(String[] args) {
		GenerateNewUserApplication generateNewUserApplication = new GenerateNewUserApplication();
		System.out.println(generateNewUserApplication.getInsertUser("guilherme.giusti04@gmail.com", "guika1234!@#$"));
		
		System.out.println(generateNewUserApplication.getInsertUser("pietra@plussim.com", "PlusSim2018"));
		System.out.println(generateNewUserApplication.getInsertUser("carolina@plussim.com", "PlusSim2018"));
		System.out.println(generateNewUserApplication.getInsertUser("brunno@plussim.com", "PlusSim2018"));
		System.out.println(generateNewUserApplication.getInsertUser("thaliton@plussim.com", "PlusSim2018"));
		
	}
	
	public String getInsertUser(String email, String password) {
		StringBuilder insert = new StringBuilder();
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		insert.append("\nINSERT INTO USER_APPLICATION (DS_USER_EMAIL, DS_USER_PASSWORD, BL_USER_STATUS)");
		insert.append("\nVALUES ('");
		insert.append(email + "', '");
		insert.append(encoder.encode(password) + "', ");
		insert.append(1 + ");");
		return insert.toString();
	}
	
}
