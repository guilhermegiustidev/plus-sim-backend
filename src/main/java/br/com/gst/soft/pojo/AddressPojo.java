package br.com.gst.soft.pojo;

import br.com.gst.soft.entity.localization.City;
import br.com.gst.soft.entity.localization.Country;
import br.com.gst.soft.entity.localization.State;
import br.com.gst.soft.exception.PojoValidationException;
import br.com.gst.soft.rules.IPojoParameter;
import lombok.Data;

@Data
public class AddressPojo implements IPojoParameter {

	private Long id;
	private String address;
	private String lat;
	private String log;
	private String zip;
	private Country country;
	private State state;
	private City city;
	private boolean status;
	private boolean primary;
	
	@Override
	public void validate() throws PojoValidationException {
	}

}
