package br.com.gst.soft.pojo;

import java.io.Serializable;
import java.util.List;

import br.com.gst.soft.response.BranchDTO;
import br.com.gst.soft.response.PartnerDTO;
import lombok.Data;

@Data
public class PersonPojo implements Serializable{

	private static final long serialVersionUID = 8265211477552070550L;
	private Long personId;
	private String personName;
	private String personLogin;
	private List<ContactPojo> contacts;
	private List<ProfileDTO> profiles;
	private List<BranchDTO> branchs;
	private List<PartnerDTO> partners;
}
