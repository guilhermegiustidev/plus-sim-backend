package br.com.gst.soft.pojo;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.util.StringUtils;

import br.com.gst.soft.exception.PojoValidationException;
import br.com.gst.soft.rules.IPojoParameter;
import lombok.Data;

@Data
public class SignUserPojo implements IPojoParameter {

	private String username;
	private String email;
	private String password;
	private String passwordConfirm;

	@Override
	public void validate() throws PojoValidationException {
		if(StringUtils.isEmpty(email)){
			throw new PojoValidationException("Email is empty", "labels.errors.email.user.is.null");
		}
		if(StringUtils.isEmpty(password)){
			throw new PojoValidationException("password is empty", "labels.errors.password.user.is.null");
		}
		if(StringUtils.isEmpty(passwordConfirm)){
			throw new PojoValidationException("passwordConfirm is empty", "labels.errors.passwordConfirm.user.is.null");
		}
		if(password.equals(passwordConfirm) == false){
			throw new PojoValidationException("Password and ConfirmPassword Should be same", "labels.errors.password.and.confirm.not.equals");
		}
		
		try {
			InternetAddress validator = new InternetAddress(this.email);
			validator.validate();
		} catch (AddressException e) {
			e.printStackTrace();
			throw new PojoValidationException("Email Inválido ", "labels.errors.email.invalido");
		}
		
	}
	
}
