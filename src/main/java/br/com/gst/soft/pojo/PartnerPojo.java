package br.com.gst.soft.pojo;

import java.io.Serializable;
import java.util.List;

import br.com.gst.soft.response.PersonDTO;
import lombok.Data;

@Data
public class PartnerPojo implements Serializable{

	private static final long serialVersionUID = 7586420072392276363L;
	private Long partnerId;
	private String partnerName;
	private List<PersonDTO> users;
	private List<AddressPojo> addresses;
	private List<ContactPojo> contacts;
	
}
