package br.com.gst.soft.pojo;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProfileDTO implements Serializable{

	private static final long serialVersionUID = 3149605188726316459L;
	private Long id;
	private String description;
	private boolean status;
	
}
