package br.com.gst.soft.pojo;

import java.util.List;

import br.com.gst.soft.response.SimResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceLotSearchPojo {

	private Long requestId;
	private Long branchId;
	private Long partnerId;
	private String lotCode;
	private List<SimResponse> sims;
	
}
