package br.com.gst.soft.pojo;

import java.io.Serializable;
import java.util.List;

import br.com.gst.soft.rules.ContactType;
import lombok.Data;

@Data
public class ContactPojo implements Serializable{

	private static final long serialVersionUID = -5374706925330180997L;
	private Long id;
	private String name;
	private ContactType type;
	private String email;
	private String phone;
	private List<LanguagesPojo> langs;
	private boolean status;
	
}
