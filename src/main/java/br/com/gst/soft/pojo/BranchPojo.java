package br.com.gst.soft.pojo;

import java.io.Serializable;
import java.util.List;

import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.response.PersonDTO;
import lombok.Data;

@Data
public class BranchPojo implements Serializable{

	private static final long serialVersionUID = 8265211477552070550L;
	private Long branchId;
	private String branchName;
	private List<PersonDTO> users;
	private List<AddressPojo> addresses;
	private List<ContactPojo> contacts;
	
}
