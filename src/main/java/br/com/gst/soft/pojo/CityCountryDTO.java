package br.com.gst.soft.pojo;

import br.com.gst.soft.exception.PojoValidationException;
import br.com.gst.soft.rules.IPojoParameter;
import lombok.Data;

@Data
public class CityCountryDTO implements IPojoParameter{

	private Long countryId;
	private Long stateId;
	private String name;
	private Integer pageNumber;
	private Integer pageSize;
	private Long count;
	
	@Override
	public void validate() throws PojoValidationException {
		// TODO Auto-generated method stub
		
	}

}
