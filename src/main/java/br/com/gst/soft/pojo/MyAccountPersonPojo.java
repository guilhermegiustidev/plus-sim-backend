package br.com.gst.soft.pojo;

import java.io.Serializable;
import java.util.Date;

import br.com.gst.soft.entity.config.ConfigLayout;
import lombok.Data;

@Data
public class MyAccountPersonPojo implements Serializable{

	private static final long serialVersionUID = 4560729371391272672L;
	private String name;
	private Date bornDate;
	private String password;
	private String confirmPassword;
	private ConfigLayout config;
	
	
}
