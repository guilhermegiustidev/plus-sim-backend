package br.com.gst.soft.pojo;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceSimDTO {
	
	private Long id;
	private Long requestId;
	private Long partnerId;
	private Long branchId;
	private Long addressId;
	private Set<Long> sims;
}
