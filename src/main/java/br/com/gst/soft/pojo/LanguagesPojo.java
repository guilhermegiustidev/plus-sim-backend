package br.com.gst.soft.pojo;

import java.io.Serializable;

import lombok.Data;

@Data
public class LanguagesPojo implements Serializable{

	private static final long serialVersionUID = 9135930940395061679L;
	private String id;
	private String title;
	private String flag;

}
