package br.com.gst.soft.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter implements Filter{

	@Value("${spring.client-adress}")
	private String origemPermitida;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		httpServletResponse.setHeader("Access-Control-Allow-Origin", origemPermitida); 
		httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true"); 

		if("OPTIONS".equals(httpServletRequest.getMethod()) && origemPermitida.equals(httpServletRequest.getHeader("Origin"))) {
			httpServletResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS"); 
			httpServletResponse.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept"); 
			httpServletResponse.setHeader("Access-Control-Max-Age", "3600"); 
			
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
		} else {
			chain.doFilter(httpServletRequest, httpServletResponse);
		}
			
		
	}

	@Override public void destroy() {}
	@Override public void init(FilterConfig filterConfig) throws ServletException {}

}
