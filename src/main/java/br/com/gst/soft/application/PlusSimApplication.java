package br.com.gst.soft.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableAsync
@ComponentScan
@EnableScheduling
@EntityScan("br.com.gst.soft.entity")
@ComponentScan("br.com.gst.soft.*")
@EnableJpaRepositories("br.com.gst.soft.repository")
@SpringBootApplication(scanBasePackages = "br.com.gst.soft.*")
public class PlusSimApplication implements CommandLineRunner {

	@Value("${spring.client-adress}")
	private String origemPermitida;
	
	public static void main(String[] args) {
		SpringApplication.run(PlusSimApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("InitAplication");
		log.info("Origem permitida, url ={}", this.origemPermitida);
		log.info("Server Started");		
	}
	
}
