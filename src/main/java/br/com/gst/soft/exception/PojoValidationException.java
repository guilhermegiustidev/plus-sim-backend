package br.com.gst.soft.exception;

import lombok.Getter;

@Getter
public class PojoValidationException extends Exception {

	private static final long serialVersionUID = -4395434902599304511L;
	private String labelMessage;
	
	public PojoValidationException (final String message, final String label){
		super(message);
		this.labelMessage = label;
	}
	
}
