package br.com.gst.soft.entity.request.sim;

public enum RequestStatus {

	OPENED,
	PROCESSING,
	REFUSED,
	WAITING_DISPATCH,
	DONE;
	
}
