package br.com.gst.soft.entity;

import java.io.Serializable;

public interface Model extends Serializable{

	public Long getId();
	
}
