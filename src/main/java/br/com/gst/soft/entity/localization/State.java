package br.com.gst.soft.entity.localization;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gst.soft.entity.Model;
import lombok.Data;

@Data
@Entity
@Table(name = "STATE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class State implements Model{

	private static final long serialVersionUID = 4598939540611806941L;

	@Id
	@Column(name = "ID_STATE")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NAME")
	private String name;
	
	@Column(name = "CD_CODE")
	private String code;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_COUNTRY")
	private Country country;
	
}
