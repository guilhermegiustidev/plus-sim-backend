package br.com.gst.soft.entity.secutiry;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="USER_TOKEN")
public class Token implements Serializable{

	private static final long serialVersionUID = 2000749368449976993L;

	@Id
	@Column(name = "ID_TOKEN")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DS_TOKEN")
	private String token;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_USER")
	private Person user;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_REFRESH_TOKEN")
	private RefreshToken refreshToken;
	
	@Column(name = "DT_CREATE")
	private Date create;

	@Column(name = "ST_ALIVE")
	private boolean alive;
	
}
