package br.com.gst.soft.entity.address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.localization.City;
import br.com.gst.soft.entity.localization.Country;
import br.com.gst.soft.entity.localization.State;
import br.com.gst.soft.rules.LogicInstance;
import lombok.Data;

@Data
@Entity
@Table(name = "ADDRESS")
public class Address implements Model, LogicInstance {

	private static final long serialVersionUID = -458477469628932332L;

	@Id
	@Column(name = "ID_ADDRESS")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DS_ADDRESS")
	private String address;

	@Column(name = "VL_LATITUDE")
	private String lat;
	
	@Column(name = "VL_LONGITUDE")
	private String log;
	
	@Column(name = "ZIP_ADDRESS")
	private String zip;
	
	@JoinColumn(name = "ID_COUNTRY")
	@ManyToOne(fetch=FetchType.LAZY)
	private Country country;
	
	@JoinColumn(name = "ID_STATE")
	@ManyToOne(fetch=FetchType.LAZY)
	private State state;
	
	@JoinColumn(name = "ID_CITY")
	@ManyToOne(fetch=FetchType.LAZY)
	private City city;
	
	private boolean status;
	
	@Column(name = "DS_COMPLEMENT")
	private String complement;
	
	@Override
	public void exclude() {
		this.status = false;
	}

}
