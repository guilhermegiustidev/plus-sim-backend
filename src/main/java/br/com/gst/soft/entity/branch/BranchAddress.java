package br.com.gst.soft.entity.branch;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.rules.AddressDependency;
import br.com.gst.soft.rules.LogicInstance;
import lombok.Data;

@Data
@Entity
@Table(name = "BRANCH_ADDRESS")
public class BranchAddress implements Model, LogicInstance, AddressDependency {

	private static final long serialVersionUID = -458477469628932332L;

	@Id
	@Column(name = "ID_BRANCH_ADDRESS")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "ID_BRANCH")
	private Long dependency;

	@Column(name = "ID_ADDRESS")
	private Long address;
	
	@Column(name = "FG_PRIMARY")
	private boolean principal;
	
	private boolean status;

	@Override
	public void exclude() {
		status = false;
	}

}
