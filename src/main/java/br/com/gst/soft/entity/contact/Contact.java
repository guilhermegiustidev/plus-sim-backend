package br.com.gst.soft.entity.contact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.rules.ContactType;
import br.com.gst.soft.rules.LogicInstance;
import lombok.Data;

@Data
@Entity
@Table(name = "CONTACT")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Contact implements Model, LogicInstance{

	private static final long serialVersionUID = -1886684517967655337L;

	@Id
	@Column(name = "ID_CONTACT")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DS_DESCRIPTION")
	private String name;
	
	@Column(name = "DS_VALUE")
	private String value;

	@Transient
	private String email;

	@Transient
	private String phone;
	
	@Column(name = "STATUS")
	private boolean status;

	@Enumerated(EnumType.STRING)
	@Column(name = "TP_TYPE_CONTACT")
	private ContactType type;

	@Override
	public void exclude() {
		this.status = false;
	}
	
	@PostLoad
	public void postLoad() {
		if(StringUtils.isEmpty(this.value) == false && this.type != null) {
			switch (this.type) {
			case EMAIL:
				this.email = this.value;
				break;
			case PHONE:
				this.phone = this.value;
				break;
			}
		}
	}
}
