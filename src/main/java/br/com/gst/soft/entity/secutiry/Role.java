package br.com.gst.soft.entity.secutiry;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="ROLE")
@ToString(exclude= {"profiles"})
@EqualsAndHashCode(exclude= {"profiles"})
public class Role implements Serializable {

	private static final long serialVersionUID = -6042649525147738008L;

	@Id
	@Column(name = "ID_ROLE")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NAME")
	private String description;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "ROLES_PROFILE"
	  , joinColumns = {@JoinColumn(name = "ID_ROLE")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_PROFILE")})
	private List<Profile> profiles;
}
