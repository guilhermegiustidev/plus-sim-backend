package br.com.gst.soft.entity.request;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.entity.request.sim.RequestStatus;
import br.com.gst.soft.entity.secutiry.Person;
import lombok.Data;

@Data
@Entity
@Deprecated
@Table(name = "REQUEST")
public class PlusSimRequest implements Model{

	private static final long serialVersionUID = -7412769523296030862L;

	@Id
	@Column(name = "ID_REQUEST")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USER")
	private Person person;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_REQUEST")
	private Date dateRequest;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "EN_STATUS")
	private RequestStatus status;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_PERFORMING")
	private Partner performing;
	
	@OneToMany(mappedBy="request", fetch=FetchType.LAZY)
	private List<PlusSimRequestDetail> details;
}
