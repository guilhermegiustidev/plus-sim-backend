package br.com.gst.soft.entity.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@Table(name="JOB_PROCESS")
@AllArgsConstructor
public class JobEntity implements Serializable {

	private static final long serialVersionUID = -8908382385352469016L;

	@Id
	@Column(name = "ID_JOB")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NAME")
	private String name;

	@Column(name = "IDENTIFIER")
	private String identifier;

	@Column(name = "NM_TRY")
	private Long turn;
	
	@Column(name = "LAST_EXECUTION")
	private Date execution;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "STATUS")
	private StatusJob status;

}
