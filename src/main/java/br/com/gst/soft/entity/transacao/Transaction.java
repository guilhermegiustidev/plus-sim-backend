package br.com.gst.soft.entity.transacao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.gst.soft.entity.secutiry.Person;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="LOG_TRANSACTION")
public class Transaction implements Serializable{

	private static final long serialVersionUID = 3626699702468410438L;

	@Id
	@Column(name = "ID_TRANSACTION")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(name = "ID_USER_REGISTER")
	private Person userRegister;
	
	@JoinColumn(name = "ID_USER_UPDATE")
	private Person userLastUpdate;

	@Column(name = "DT_REGISTER")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateRegister;
	
	@Column(name = "DT_LAST_UPDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateLastUpdate;
	
}
