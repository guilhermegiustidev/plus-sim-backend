package br.com.gst.soft.entity.secutiry;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.rules.HasContacts;
import br.com.gst.soft.rules.LogicInstance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="PERSON")
@ToString(exclude = {"profiles", "contacts"})
public class Person implements Model, HasContacts, LogicInstance {

	private static final long serialVersionUID = -8944952913622331682L;

	@Id
	@Column(name = "ID_PERSON")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NAME")
	private String name;
	
	@Column(name = "CD_LOGIN")
	private String login;
	
	@Column(name = "CD_PASSWORD")
	private String password;
	
	private boolean status;
	
	@Column(name = "ID_FACEBOOK")
	private String facebook;

	@Lob
	@Column(name = "IM_AVATAR")
	private String avatar;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name = "USER_PROFILE"
	  , joinColumns = {@JoinColumn(name = "ID_USER")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_PROFILE")})
	private Set<Profile>  profiles;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "PERSON_CONTACT"
	  , joinColumns = {@JoinColumn(name = "ID_PERSON")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_CONTACT")})
	private Set<Contact> contacts;

	@Override
	public void exclude() {
		this.status = false;
	}
	
}
