package br.com.gst.soft.entity.branch;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.rules.HasContacts;
import br.com.gst.soft.rules.HasUsers;
import br.com.gst.soft.rules.LogicInstance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "BRANCH")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Branch implements Model, LogicInstance, HasContacts, HasUsers{

	private static final long serialVersionUID = -4289822799076405730L;

	@Id
	@Column(name = "ID_BRANCH")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NAME")
	private String name;

	@Column(name = "STATUS")
	private boolean status;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "BRANCH_CONTACT"
	  , joinColumns = {@JoinColumn(name = "ID_BRANCH")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_CONTACT")})
	private Set<Contact> contacts;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "BRANCH_USER"
	  , joinColumns = {@JoinColumn(name = "ID_BRANCH")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_PERSON")})
	private Set<Person> persons;
	
	
	@Override
	public void exclude() {
		this.status = false;
	}
}
