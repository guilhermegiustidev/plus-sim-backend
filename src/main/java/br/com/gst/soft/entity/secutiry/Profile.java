package br.com.gst.soft.entity.secutiry;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.rules.LogicInstance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="PROFILE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "roles", "modules", "program"})
public class Profile implements Model, LogicInstance {

	private static final long serialVersionUID = 7335962402481709214L;

	@Id
	@Column(name = "ID_PROFILE")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NAME")
	private String description;

	private boolean status;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name = "ROLES_PROFILE"
	  , joinColumns = {@JoinColumn(name = "ID_PROFILE")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_ROLE")})
	private List<Role> roles;
		
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "USER_PROFILE"
	  , joinColumns = {@JoinColumn(name = "ID_PROFILE")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_USER")})
	private List<Person>  users;

	
	@Override
	public void exclude() {
		this.status = false;
	}
	
}
