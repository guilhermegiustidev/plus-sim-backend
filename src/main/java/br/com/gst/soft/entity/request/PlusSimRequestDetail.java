package br.com.gst.soft.entity.request;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.servicos.Plan;
import lombok.Data;

@Data
@Entity
@Table(name = "REQUEST_DETAILS")
@Deprecated
public class PlusSimRequestDetail implements Model{

	private static final long serialVersionUID = 4431621880667074569L;

	@Id
	@Column(name = "ID_REQUEST_DETAIL")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_REQUEST")
	private PlusSimRequest request;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_CLIENT")
	private Person person;
	
	@OneToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "SIN_PLAN_REQUEST_DETAIL", joinColumns=@JoinColumn(name = "ID_REQUEST_DETAIL"), inverseJoinColumns=@JoinColumn(name="ID_SIN_PLAN"))
	private List<Plan> plans;
}
