package br.com.gst.soft.entity.sim;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.gst.soft.entity.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Table(name = "PLUS_CHIP_SIM_LOT")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SimLot implements Model {

	private static final long serialVersionUID = 5074142956950588586L;

	@Id
	@Column(name = "ID_PLUS_SIM_LOT_CHIP")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "ID_SIM")
	private Sim sim;
	
	@ManyToOne
	@JoinColumn(name = "ID_LOT")
	private Lot lot;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_TIME_INCLUDE")
	private Date include;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_TIME_LEAVE")
	private Date leave;
	
}
