package br.com.gst.soft.entity.line;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.localization.City;
import br.com.gst.soft.entity.localization.Country;
import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.servicos.PlanProvider;
import lombok.Data;

@Data
@Entity
@Table(name = "SIN_LINE")
public class Line implements Model{

	private static final long serialVersionUID = -3218083887247276513L;

	@Id
	@Column(name = "ID_SIN")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NUMBER")
	private String number;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "TP_STATUS")
	private LineStatus status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_RELEASED")
	private Date released;
	
	@JoinColumn(name = "ID_COUNTRY")
	@ManyToOne(fetch=FetchType.LAZY)
	private Country country;
	
	@JoinColumn(name = "ID_CITY")
	@ManyToOne(fetch=FetchType.LAZY)
	private City city;
	
	@JoinColumn(name = "ID_PROVIDER")
	@ManyToOne(fetch=FetchType.LAZY)
	private Provider provider;

	@JoinColumn(name = "ID_PROVIDER_PLAN")
	@ManyToOne(fetch=FetchType.LAZY)
	private PlanProvider providerPlan;
	
}
