package br.com.gst.soft.entity.servicos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.rules.LogicInstance;
import lombok.Data;

@Data
@Entity
@Table(name = "PLAN_SERVICE_TYPE")
public class PlanServiceType implements Model, LogicInstance {

	private static final long serialVersionUID = 427244083656092129L;

	@Id
	@Column(name = "ID_SERVICE_TYPE")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DS_NAME")
	private String name;

	private boolean status;
	
	@Override
	public void exclude() {
		this.status = false;
	}

	
	
}
