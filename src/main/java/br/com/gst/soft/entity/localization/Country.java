package br.com.gst.soft.entity.localization;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gst.soft.entity.Model;
import lombok.Data;

@Data
@Entity
@Table(name = "COUNTRY")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Country implements Model{

	private static final long serialVersionUID = 8495569239720483814L;

	@Id
	@Column(name = "ID_COUNTRY")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NAME")
	private String name;
	
	@Column(name = "CD_CODE")
	private String code;
	
	@Column(name = "LOCALE")
	private String locale;
	
}
