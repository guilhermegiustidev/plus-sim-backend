package br.com.gst.soft.entity.transacao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.gst.soft.entity.secutiry.Person;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="LOG_TRANSACTION_DETAIL")
public class TransactionDetail implements Serializable{

	private static final long serialVersionUID = 3626699702468410438L;

	@Id
	@Column(name = "ID_TRANSACTION_DETAIL")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(name = "ID_USER_APPLICATION")
	private Person userRegister;
	
	@Column(name = "DT_TRANSACTION_DETAIL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateRegister;
	
	@JoinColumn(name = "ID_TRANSACTION")
	private Transaction transaction;
	
	@Column(name = "DATA_PREVIOUS")
	private String previousInstance;
	
	@Column(name = "DATA_CURRENT")
	private String currentInstance;
	
}
