package br.com.gst.soft.entity.request.sim;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.address.Address;
import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.entity.secutiry.Person;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "REQUEST_SIM_CHIP")
public class RequestSimChip implements Model	{

	private static final long serialVersionUID = 8626230522873959447L;

	@Id
	@Column(name = "ID_REQUEST_SIM_CHIP")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DT_REQUEST")
	@Temporal(TemporalType.TIMESTAMP)
	private Date bornDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PERSON")
	private Person personRequest;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PARTNER")
	private Partner partner;

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private RequestStatus status;

	@OneToMany(mappedBy = "requestSim", fetch=FetchType.EAGER)
	private List<RequestItemSimChip> items;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ADDRESS")
	private Address address;

	@Transient
	private Integer quantity;
	
	@Transient
	private BigDecimal price;
}
