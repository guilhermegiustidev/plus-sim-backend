package br.com.gst.soft.entity.sim;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.secutiry.Person;
import lombok.Data;

@Data
@Entity
@Table(name = "SIN_LOT")
public class Lot implements Model{

	private static final long serialVersionUID = -7198826274504391123L;

	@Id
	@Column(name = "ID_LOT")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "CD_CODE")
	private String code;
	
	@Enumerated(EnumType.STRING)
	private LotStatus status;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_PERSON")
	private Person person;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_BRANCH")
	private Branch branch;
	
}
