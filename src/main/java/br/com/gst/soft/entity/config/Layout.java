package br.com.gst.soft.entity.config;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class Layout {
	
	@Column(name = "LAYOUT_FOOTER")
	private String footer;

	@Column(name = "LAYOUT_MODE")
	private String mode;

	@Column(name = "LAYOUT_NAVIGATION")
	private String navigation;

	@Column(name = "LAYOUT_NAV_FOLDED")
	private boolean navigationFolded;

	@Column(name = "LAYOUT_TOOLBAR")
	private String toolbar;
}
