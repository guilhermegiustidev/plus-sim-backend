package br.com.gst.soft.entity.request.invoice;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.address.Address;
import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.entity.secutiry.Person;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INVOICE_SIM_CHIP")
public class InvoiceRequestChip implements Model{

	private static final long serialVersionUID = -8436820287868538152L;

	@Id
	@Column(name = "ID_INVOICE")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(name = "ID_REQUEST")
	@OneToOne(fetch=FetchType.LAZY)
	private RequestSimChip request;

	@JoinColumn(name = "ID_PERSON")
	@OneToOne(fetch=FetchType.LAZY)
	private Person person;
	
	@JoinColumn(name = "ID_BRANCH")
	@OneToOne(fetch=FetchType.LAZY)
	private Branch branch;

	@JoinColumn(name = "ID_PARTNER")
	@OneToOne(fetch=FetchType.LAZY)
	private Partner partner;

	@JoinColumn(name = "ID_ADDRESS")
	@OneToOne(fetch=FetchType.LAZY)
	private Address address;

	@Column(name = "DT_BORN")
	@Temporal(TemporalType.TIMESTAMP)
	private Date bornDate;
	
	@OneToMany(mappedBy = "invoice")
	private List<InvoiceRequestChipItem> items;
	
}
