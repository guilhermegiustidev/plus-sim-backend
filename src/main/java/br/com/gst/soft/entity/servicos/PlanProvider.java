package br.com.gst.soft.entity.servicos;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.provider.Provider;
import lombok.Data;

@Data
@Entity
@Table(name = "PROVIDER_PLAN")
public class PlanProvider implements Model{

	private static final long serialVersionUID = 4715356521960802696L;

	@Id
	@Column(name = "ID_PROVIDER_PLAN")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "DS_NAME")
	private String name;

	@Column(name = "STATUS")
	private boolean status;
	
	@Column(name = "VL_COST")
	private BigDecimal cost;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_PROVIDER")
	private Provider provider;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name = "PLAN_PROVIDER_SERVICES"
	  , joinColumns = {@JoinColumn(name = "ID_PROVIDER_PLAN")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_SERVICE")})
	private Set<PlanServiceType> services;
}
