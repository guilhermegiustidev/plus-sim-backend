package br.com.gst.soft.entity.config;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class ColorClasses {
	
	@Column(name = "COLOR_FOOTER")
	private String footer;

	@Column(name = "COLOR_NAV")
	private String navbar;
	
	@Column(name = "COLOR_TOOLBAR")
	private String toolbar;
	
}
