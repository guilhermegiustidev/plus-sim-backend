package br.com.gst.soft.entity.secutiry;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public enum Menu {

	ADM, 
	
		OPERATIONAL(Menu.ADM), 
			ADM01(Menu.OPERATIONAL), 
			ADM04(Menu.OPERATIONAL), 
			ADM10(Menu.OPERATIONAL),
	
		ACCESS_AND_SECURITY(Menu.ADM), 
			ADM02(Menu.ACCESS_AND_SECURITY), 
			ADM06(Menu.ACCESS_AND_SECURITY),
	
		FINNANCIAL(Menu.ADM),
	
		E_COMMERCE(Menu.ADM),
			ADM08(Menu.E_COMMERCE),
	
		STOCK(Menu.ADM),
			ADM12(Menu.STOCK),
			ADM14(Menu.STOCK),
			
		SERVICES(Menu.ADM),
			ADM03(Menu.SERVICES),
			PLAN(Menu.SERVICES),
				ADM05(Menu.PLAN),
				ADM09(Menu.PLAN),
				ADM11(Menu.PLAN),
			ADM07(Menu.SERVICES),
		
		ORDER_PARTNER(Menu.ADM),
			ORD03(Menu.ORDER_PARTNER),
			ORD04(Menu.ORDER_PARTNER),
	PAT,
	
		ORDER_PAT(Menu.PAT),
			ORD01(Menu.ORDER_PAT),
			ORD02(Menu.ORDER_PAT),
	
		FINNANCIAL_PARTNER(Menu.PAT),
		
		STOCK_PARTNER(Menu.PAT),
			ADM15(Menu.STOCK_PARTNER),
			ADM16(Menu.STOCK_PARTNER);

	@Getter
	private Menu parent;

	public static List<Menu> findByParent(Menu parent){
		List<Menu> menus = new ArrayList<Menu>();
		for(Menu menu : Menu.values()) {
			Menu parentIndex = menu.getParent();
			if(parent == null && parentIndex == null) {
				menus.add(menu);
			} else if (parentIndex != null && parent != null && parent.equals(parentIndex)) {
				menus.add(menu);
			}
		}
		return menus;
	}

	public List<Menu> findChildren(){
		List<Menu> menus = new ArrayList<Menu>();
		for(Menu menu : Menu.values()) {
			Menu parentIndex = menu.getParent();
			if (parentIndex != null && this.equals(parentIndex)) {
				menus.add(menu);
			}
		}
		return menus;
	}
	
	Menu(Menu parent) {
		this.parent = parent;
	}

	Menu() {
		this(null);
	}

}
