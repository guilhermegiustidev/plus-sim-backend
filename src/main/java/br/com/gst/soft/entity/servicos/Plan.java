package br.com.gst.soft.entity.servicos;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.gst.soft.entity.Model;
import lombok.Data;

@Data
@Entity
@Table(name = "SIN_PLAN")
public class Plan implements Model{

	private static final long serialVersionUID = -4596420368608381503L;

	@Id
	@Column(name = "ID_PLAN")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "VL_PRICE")
	private BigDecimal price;

	@Column(name = "VL_PROFIT")
	private BigDecimal valorProfit;

	@Column(name = "HR_DURATION")
	private Long minutes;
	
	@Transient
	private Long days;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name = "PLAN_SERVICES"
	  , joinColumns = {@JoinColumn(name = "ID_PLAN")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_SERVICE")})
	private Set<PlanServiceType> services;
	
	@PostLoad
	public void postLoad() {
		if(this.minutes != null) {
			this.days = (this.minutes / 60) / 24;
		}
	}
	
}
