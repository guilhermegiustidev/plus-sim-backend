package br.com.gst.soft.entity.secutiry;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="REFRESH_TOKEN")
@ToString(exclude= {"tokens"})
public class RefreshToken implements Serializable{

	private static final long serialVersionUID = -3083139499906678395L;

	@Id
	@Column(name = "ID_REFRESH_TOKEN")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DS_REFRESH_TOKEN")
	private String token;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PERSON")
	private Person user;
	
	@Column(name = "DT_CREATE")
	private Date create;

	@Column(name = "ST_ALIVE")
	private boolean alive;

	@OneToMany(fetch=FetchType.EAGER, mappedBy="refreshToken")
	private List<Token> tokens;
	
}
