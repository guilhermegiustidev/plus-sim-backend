package br.com.gst.soft.entity.provider;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.rules.LogicInstance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PROVIDER")
public class Provider implements Model, LogicInstance {

	private static final long serialVersionUID = -6303586514155663085L;

	@Id
	@Column(name = "ID_PROVIDER")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DS_NAME")
	private String name;

	private boolean status;
	
	@Column(name = "CODE_REGEX") 
	private String regex;

	@Column(name = "VL_SIM_CHIP_BUY") 
	private BigDecimal valorChipCompra;

	@Column(name = "VL_SIM_CHIP_SELL") 
	private BigDecimal valorChipVenda;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "PROVIDER_CONTACT"
	  , joinColumns = {@JoinColumn(name = "ID_PROVIDER")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_CONTACT")})
	private Set<Contact> contacts;
	
	@Override
	public void exclude() {
		this.status = false;
	}

	
	
}
