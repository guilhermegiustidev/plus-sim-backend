package br.com.gst.soft.entity.servicos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.ibatis.annotations.Many;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.localization.City;
import br.com.gst.soft.entity.localization.Country;
import br.com.gst.soft.rules.LogicInstance;
import lombok.Data;

@Data
@Entity
@Table(name = "PABX")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pabx implements Model, LogicInstance{

	private static final long serialVersionUID = -2946738318243693244L;

	public enum PabxStatus{
		FREE,
		BUSY;
	}
	
	@Id
	@Column(name = "ID_PABX")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "NB_LINE")
	private String numberLine;
	
	@Column(name = "STATUS")
	private boolean status;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_COUNTRY")
	private Country country;

	@JoinColumn(name = "ID_CITY")
	@ManyToOne(fetch=FetchType.LAZY)
	private City city;

	@Transient
	private PabxStatus statusEnum;
	
	@Override
	public void exclude() {
		this.status = false;
	}
}
