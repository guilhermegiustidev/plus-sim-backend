package br.com.gst.soft.entity.request.invoice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.sim.Sim;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INVOICE_SIM_CHIP_ITEM")
@JsonIgnoreProperties({"invoice"})
public class InvoiceRequestChipItem implements Model{

	private static final long serialVersionUID = 3996084615853832705L;

	@Id
	@Column(name = "ID_INVOICE_ITEM")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(name = "ID_INVOICE")
	@ManyToOne(fetch = FetchType.LAZY)
	private InvoiceRequestChip invoice;
	
	@JoinColumn(name = "ID_SIM")
	@ManyToOne(fetch = FetchType.LAZY)
	private Sim sim;
}
