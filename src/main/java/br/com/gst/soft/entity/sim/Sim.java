package br.com.gst.soft.entity.sim;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.provider.Provider;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Table(name = "SIM_CHIP")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Sim implements Model{

	public enum SimChipStatus {
		FREE,
		
		PARTNER,
		
		RESERVED_INVOICE,
		
		USED;
	}
	
	
//	8901260963189522860F
//^(89012)?([0-9]{19})?[fF]$
	//^(89012)([0-9]{14})([fF])$
	private static final long serialVersionUID = -6135871307718726983L;

	@Id
	@Column(name = "ID_SIN")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "CD_CODE")
	private String code;
//	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROVIDER")
	private Provider provider;	
	
	@Column(name = "REGEX")
	private String regex;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "ST_STATUS")
	private SimChipStatus status;
}
