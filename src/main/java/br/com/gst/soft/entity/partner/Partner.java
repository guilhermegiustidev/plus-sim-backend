package br.com.gst.soft.entity.partner;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.contact.Contact;
import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.rules.HasContacts;
import br.com.gst.soft.rules.HasUsers;
import br.com.gst.soft.rules.LogicInstance;
import lombok.Data;

@Data
@Entity
@Table(name = "PARTNER")
public class Partner implements Model, LogicInstance, HasContacts, HasUsers {

	private static final long serialVersionUID = -458477469628932332L;

	@Id
	@Column(name = "ID_PARTNER")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DS_NAME")
	private String name;

	private boolean status;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "PARTNER_CONTACT"
	  , joinColumns = {@JoinColumn(name = "ID_PARTNER")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_CONTACT")})
	private Set<Contact> contacts;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "PARTNER_USER"
	  , joinColumns = {@JoinColumn(name = "ID_PARTNER")}
	  , inverseJoinColumns = {@JoinColumn(name = "ID_PERSON")})
	private Set<Person> persons;
	
	@Override
	public void exclude() {
		this.status = false;
	}

}
