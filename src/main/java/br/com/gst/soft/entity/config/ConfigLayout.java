package br.com.gst.soft.entity.config;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.gst.soft.entity.Model;
import br.com.gst.soft.entity.secutiry.Person;
import lombok.Data;

@Data
@Entity
@Table(name = "CONFIG_LAYOUT")
public class ConfigLayout implements Model {

	private static final long serialVersionUID = -2234381092018047741L;

	@Id
	@Column(name = "ID_CONFIG")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PERSON")
	private Person person;
	
	@Embedded
	private ColorClasses colorClasses;

	@Column(name = "BL_CUSTOM_SCROLL_BARS")
	private boolean customScrollbars;
	
	@Column(name = "ST_ROUTER_ANIMATION")
	private String routerAnimation;
	
	@Embedded
	private Layout layout;
}
