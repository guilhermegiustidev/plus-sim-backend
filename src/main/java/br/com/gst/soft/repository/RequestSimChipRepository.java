package br.com.gst.soft.repository;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.entity.request.sim.RequestStatus;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface RequestSimChipRepository extends JpaRepository<RequestSimChip, Long> {

	public List<RequestSimChip> findByStatus(RequestStatus status);
	
}
