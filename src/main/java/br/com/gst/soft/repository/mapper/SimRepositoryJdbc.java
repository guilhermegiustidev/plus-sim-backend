package br.com.gst.soft.repository.mapper;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

@Repository
@MapperScan
public interface SimRepositoryJdbc {

	public Long countChipsByProvider(@Param("branchId") Long branchId, @Param("providerId") Long providerId);
	
}
