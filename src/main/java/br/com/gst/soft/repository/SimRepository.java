package br.com.gst.soft.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.Sim.SimChipStatus;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface SimRepository extends JpaRepository<Sim, Long> {

	public Optional<Sim> findByCode(final String code);
	
}


