package br.com.gst.soft.repository;

import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.secutiry.Token;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface TokenRepository extends JpaRepository<Token, Long> {

	public Optional<Token> findByTokenAndUser(String token, Person user);
	public Optional<Token> findByToken(String token);
	
}
