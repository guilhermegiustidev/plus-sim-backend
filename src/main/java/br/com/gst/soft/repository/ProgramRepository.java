package br.com.gst.soft.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.secutiry.Profile;
import br.com.gst.soft.entity.secutiry.Program;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ProgramRepository extends JpaRepository<Program, Long> {
	
	List<Program> findByProfile(Profile profile);

	Optional<Program> findByName(String name);
	
	Optional<Program> findByNameAndProfile(String name, Profile profile);
	
}
