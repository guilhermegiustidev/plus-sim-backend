package br.com.gst.soft.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.localization.City;

@Repository
@MapperScan
public interface LocationJdbcRepository {

	public List<City> findCitiesByCountryId(@Param("countryId") Long countryId);
	
}
