package br.com.gst.soft.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.partner.PartnerAddress;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface PartnerAddressRepository extends JpaRepository<PartnerAddress, Long> {

	@Query("SELECT ba FROM PartnerAddress ba WHERE ba.address= ?1 and ba.dependency = ?2")
	public Optional<PartnerAddress> findByAddressAndPartner(Long address, Long partner);

	public List<PartnerAddress> findByDependency(Long dependencyId);
	
}
