package br.com.gst.soft.repository;

import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.sim.Lot;
import br.com.gst.soft.entity.sim.LotStatus;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface LotRepository extends JpaRepository<Lot, Long> {

	public Optional<Lot> findByPersonAndStatus(Person person, LotStatus status);
	
	public Optional<Lot> findByCode(String code);

}
