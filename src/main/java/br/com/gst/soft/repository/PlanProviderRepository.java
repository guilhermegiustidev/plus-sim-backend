package br.com.gst.soft.repository;

import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.servicos.PlanProvider;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface PlanProviderRepository extends JpaRepository<PlanProvider, Long> {

	Set<PlanProvider> findByProvider(Provider provider);

}
