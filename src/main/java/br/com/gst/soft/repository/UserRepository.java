package br.com.gst.soft.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.secutiry.Person;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface UserRepository extends JpaRepository<Person, Long> {

	@Query("Select p from Person p where p.name LIKE  %?1%")
	public Set<Person> findByLikeName(String name);

	public Optional<Person> findByLogin(String login);

	@Query("Select CASE WHEN count(p) > 0 THEN TRUE ELSE FALSE END from Person p where p.login = ?1")
	public boolean verifyLogin(String login);
	
	
}
