package br.com.gst.soft.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.sim.Lot;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.SimLot;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface SimLotRepository extends JpaRepository<SimLot, Long>{

	Optional<SimLot> findBySimAndLot(Sim sim, Lot lot);
	List<SimLot> findByLot(Lot lot);
	Optional<SimLot> findBySim(Sim sim);

}
