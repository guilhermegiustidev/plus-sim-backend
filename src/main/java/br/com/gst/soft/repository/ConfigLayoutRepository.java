package br.com.gst.soft.repository;

import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.config.ConfigLayout;
import br.com.gst.soft.entity.secutiry.Person;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ConfigLayoutRepository extends JpaRepository<ConfigLayout, Long> {

	public Optional<ConfigLayout> findByPerson(Person person);
	
}
