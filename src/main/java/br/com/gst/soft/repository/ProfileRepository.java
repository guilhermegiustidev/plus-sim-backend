package br.com.gst.soft.repository;

import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.secutiry.Profile;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ProfileRepository extends JpaRepository<Profile, Long> {

	public Profile findByDescription(final String description);
	
	@Query("Select p from Profile p where p.description LIKE  %?1%")
	public Set<Profile> findByLikeName(String name);
	
}
