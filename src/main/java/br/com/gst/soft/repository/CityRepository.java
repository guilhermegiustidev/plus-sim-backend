package br.com.gst.soft.repository;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.localization.City;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface CityRepository extends JpaRepository<City, Long> {

	List<City> findByStateId(Long id);

	@Query("SELECT c FROM City c WHERE c.state.country.id= ?1")
	List<City> findCitiesByCountryId(Long countryId);
	
}
