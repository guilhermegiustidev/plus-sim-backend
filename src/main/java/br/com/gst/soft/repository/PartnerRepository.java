package br.com.gst.soft.repository;

import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.partner.Partner;
import br.com.gst.soft.entity.secutiry.Person;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface PartnerRepository extends JpaRepository<Partner, Long> {

	Set<Partner> findByPersons(Person person);

	
	
}
