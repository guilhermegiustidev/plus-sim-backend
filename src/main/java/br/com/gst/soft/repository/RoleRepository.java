package br.com.gst.soft.repository;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.secutiry.Role;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface RoleRepository extends JpaRepository<Role, Long> {

	@Query("SELECT r FROM Role r WHERE r.profiles IS EMPTY")
	public List<Role> searchRolesWithoutProfile();

	@Query("SELECT r FROM Role r WHERE r.description NOT IN ?1")
	public List<Role> findRolesNotInDescription(String[] description);
}
