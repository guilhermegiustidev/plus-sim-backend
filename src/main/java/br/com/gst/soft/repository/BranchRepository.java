package br.com.gst.soft.repository;

import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.secutiry.Person;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface BranchRepository extends JpaRepository<Branch, Long> {

	public Set<Branch> findByPersons(Person person);
	
}
