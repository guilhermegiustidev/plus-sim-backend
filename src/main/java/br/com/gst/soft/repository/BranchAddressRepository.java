package br.com.gst.soft.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.branch.BranchAddress;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface BranchAddressRepository extends JpaRepository<BranchAddress, Long> {

	@Query("SELECT ba FROM BranchAddress ba WHERE ba.address= ?1 and ba.dependency = ?2")
	public Optional<BranchAddress> findByAddressAndBranch(Long address, Long branch);

	public List<BranchAddress> findByDependency(Long dependencyId);
	
}
