package br.com.gst.soft.repository;

import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.gst.soft.entity.servicos.PlanServiceType;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface PlanServiceTypeRepository extends JpaRepository<PlanServiceType, Long> {

	@Query("Select p from PlanServiceType p where p.name LIKE  %?1%")
	Set<PlanServiceType> findByLikeName(String name);
	
}
