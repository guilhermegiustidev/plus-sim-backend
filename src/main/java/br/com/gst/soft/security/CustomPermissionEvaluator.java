package br.com.gst.soft.security;

import java.io.Serializable;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import br.com.gst.soft.rules.HasGenericalImplementation;

public class CustomPermissionEvaluator implements PermissionEvaluator {
	
    @Override
    public boolean hasPermission(Authentication auth, Object targetDomainObject, Object permission) {
        if ((auth == null) || (targetDomainObject == null) || !(permission instanceof String)){
            return false;
        }
        if(targetDomainObject instanceof HasGenericalImplementation) {
        	Class<Object> clazz = ((HasGenericalImplementation) targetDomainObject).getClazz();
        	return hasPrivilege(auth, clazz.getSimpleName().toUpperCase(), permission.toString().toUpperCase());
        }
        return false;
    }
 
    @Override
    public boolean hasPermission(Authentication auth, Serializable targetId, String targetType, Object permission) {
        if ((auth == null) || (targetType == null) || !(permission instanceof String)) {
            return false;
        }
        return hasPrivilege(auth, targetType.toUpperCase(), permission.toString().toUpperCase());
    }
    
    private boolean hasPrivilege(Authentication auth, String targetType, String permission) {
        for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
            if (grantedAuth.getAuthority().startsWith("ROLE_" + targetType)) {
                if (grantedAuth.getAuthority().endsWith(permission)) {
                    return true;
                }
            }
        }
        return false;
    }
}