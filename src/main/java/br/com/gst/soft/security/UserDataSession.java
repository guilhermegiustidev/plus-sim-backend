package br.com.gst.soft.security;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.stereotype.Service;

import br.com.gst.soft.entity.secutiry.Person;
import br.com.gst.soft.entity.secutiry.Token;
import br.com.gst.soft.repository.TokenRepository;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserDataSession {

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private TokenRepository tokenRepository;
	
	private BearerTokenExtractor barerTokenExtractor;
	
	public UserDataSession() {
		this.barerTokenExtractor = new BearerTokenExtractor();
	}
	
	public Person getUserLogged() {
		Authentication extract = barerTokenExtractor.extract(httpServletRequest);
		Optional<Token> tokenOptional = tokenRepository.findByToken(extract.getPrincipal().toString());
		Token token = tokenOptional.orElseThrow(() -> new RuntimeException("Erro ao obter usuario logado"));
		return token.getUser();
	}
	
}
