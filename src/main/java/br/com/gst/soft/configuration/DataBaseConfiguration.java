package br.com.gst.soft.configuration;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.flywaydb.core.Flyway;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataBaseConfiguration {

//	@Autowired
//	private DataSource datasource;
	
	@Bean
	public MapperScannerConfigurer BPMapperScannerConfigurer() {
	    MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
	    mapperScannerConfigurer.setBasePackage("br.com.gst.soft.repository.mapper");
	    mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
	    return mapperScannerConfigurer;
	}
	
	@Bean
	public Flyway flyway(DataSource datasource) {
		Flyway fly = new Flyway();
		fly.setLocations("classpath:db/migration/mysql");
		fly.setDataSource(datasource);
		fly.setBaselineOnMigrate(true);
		fly.setOutOfOrder(true);
		fly.migrate();
		return fly;
	}
	
	
	@Bean(name="sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory(DataSource datasource) throws Exception {
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(datasource);
		return (SqlSessionFactory) sqlSessionFactory.getObject();
	}

	@Bean
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
	
}
