package br.com.gst.soft.simplus.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

import br.com.gst.soft.entity.branch.Branch;
import br.com.gst.soft.entity.provider.Provider;
import br.com.gst.soft.entity.request.sim.RequestItemSimChip;
import br.com.gst.soft.entity.request.sim.RequestSimChip;
import br.com.gst.soft.entity.request.sim.RequestStatus;
import br.com.gst.soft.entity.sim.Sim;
import br.com.gst.soft.entity.sim.Sim.SimChipStatus;
import br.com.gst.soft.repository.BranchRepository;
import br.com.gst.soft.repository.RequestItemSimChipRepository;
import br.com.gst.soft.repository.RequestSimChipRepository;
import br.com.gst.soft.repository.SimRepository;
import br.com.gst.soft.response.invoice.sim.ValidationInvoiceChipResponse;
import br.com.gst.soft.response.invoice.sim.ValidationInvoiceChipStatus;
import br.com.gst.soft.service.RequestSimChipService;

@RunWith(MockitoJUnitRunner.class)
public class RequestSimChipServiceTest {

	@InjectMocks
	private RequestSimChipService service;
	
	@Mock
	private RequestSimChipRepository repository;
	
	@Mock
	private RequestItemSimChipRepository requestItemSimChipRepository; 

	@Mock
	private SimRepository simRepository;
	
	@Mock
	private BranchRepository branchRepository;
	
	private BigDecimal valorVendaUnitarioProvider1 = BigDecimal.ONE;
	private BigDecimal valorVendaUnitarioProvider2 = BigDecimal.valueOf(1.25);
	
	private Provider provider1;
	private Provider provider2;
	private List<RequestItemSimChip> items = new ArrayList<RequestItemSimChip>();
	private ExampleMatcher matcher;
	
	//TODO Sempre virá 0 pois trata-se de um count
	private Long quantityInStockProvider1;
	private Long quantityInStockProvider2;
	
	private RequestSimChip sim = RequestSimChip.builder().id(1L).items(items).status(RequestStatus.OPENED).build();
	@Before
	public void before() {
		matcher = ExampleMatcher.matchingAll();
		provider1 = Provider.builder().id(1L).valorChipVenda(this.valorVendaUnitarioProvider1).build();
		provider2 = Provider.builder().id(2L).valorChipVenda(this.valorVendaUnitarioProvider2).build();
		items.add(RequestItemSimChip.builder().provider(provider1).build());
		items.add(RequestItemSimChip.builder().provider(provider2).build());
		Mockito.when(repository.getOne(1L)).thenReturn(sim);
		Mockito.when(branchRepository.findById(1L)).thenReturn(Optional.of(Branch.builder().id(1L).build()));
		
	}

	@Test
	public void testValidateQuantitySimpleOutOfStockAll() {
		this.quantityInStockProvider1 = 10L;
		this.quantityInStockProvider2 = 8L;
		Integer quantidadeItem1 = 30;
		Integer quantidadeItem2 = 50;
		buildAttributsTest(quantidadeItem1, quantidadeItem2);
		
		ValidationInvoiceChipResponse response = service.validateRequestSimItems(1L, 1l);
		BigDecimal valueInvoice = calcValueInvoice(quantidadeItem1, quantidadeItem2);
		Assert.assertTrue(ValidationInvoiceChipStatus.INCOMPLETE.equals(response.getStatus()));
		Assert.assertEquals(response.getInvoiceValue(), valueInvoice);
	}

	@Test
	public void testValidateQuantitySimpleOutOfStockFirst() {
		this.quantityInStockProvider1 = 10L;
		this.quantityInStockProvider2 = 8L;
		Integer quantidadeItem1 = 8;
		Integer quantidadeItem2 = 50;
		buildAttributsTest(quantidadeItem1, quantidadeItem2);
		
		ValidationInvoiceChipResponse response = service.validateRequestSimItems(1L, 1l);
		BigDecimal valueInvoice = calcValueInvoice(quantidadeItem1, quantidadeItem2);
		Assert.assertTrue(ValidationInvoiceChipStatus.INCOMPLETE.equals(response.getStatus()));
		Assert.assertEquals(response.getInvoiceValue(), valueInvoice);
	}

	@Test
	public void testValidateQuantitySimpleValidRequest() {
		this.quantityInStockProvider1 = 10L;
		this.quantityInStockProvider2 = 8L;
		Integer quantidadeItem1 = 8;
		Integer quantidadeItem2 = 6;
		buildAttributsTest(quantidadeItem1, quantidadeItem2);
		
		ValidationInvoiceChipResponse response = service.validateRequestSimItems(1L, 1l);
		BigDecimal valueInvoice = calcValueInvoice(quantidadeItem1, quantidadeItem2);
		Assert.assertTrue(ValidationInvoiceChipStatus.COMPLETE.equals(response.getStatus()));
		Assert.assertEquals(response.getInvoiceValue(), valueInvoice);
	}
	
	@Test
	public void testValidateQuantitySimpleNullValues() {
		this.quantityInStockProvider1 = 10L;
		this.quantityInStockProvider2 = 8L;
		Integer quantidadeItem1 = null;
		Integer quantidadeItem2 = 6;
		buildAttributsTest(quantidadeItem1, quantidadeItem2);
		
		ValidationInvoiceChipResponse response = service.validateRequestSimItems(1L, 1l);
		BigDecimal valueInvoice = calcValueInvoice(quantidadeItem1, quantidadeItem2);
		Assert.assertTrue(ValidationInvoiceChipStatus.COMPLETE.equals(response.getStatus()));
		Assert.assertEquals(response.getInvoiceValue(), valueInvoice);
	}
	
	@Test
	public void testValidateQuantitySimpleAllNullValues() {
		this.quantityInStockProvider1 = 0L;
		this.quantityInStockProvider2 = 0L;
		Integer quantidadeItem1 = null;
		Integer quantidadeItem2 = null;
		buildAttributsTest(quantidadeItem1, quantidadeItem2);
		
		ValidationInvoiceChipResponse response = service.validateRequestSimItems(1L, 1l);
		Assert.assertTrue(ValidationInvoiceChipStatus.COMPLETE.equals(response.getStatus()));
		Assert.assertEquals(response.getInvoiceValue(), BigDecimal.ZERO);
	}
	
	private BigDecimal calcValueInvoice(Integer quantidadeItem1, Integer quantidadeItem2) {
		Double firstValue = 0D;
		Double secondValue = 0D;

		if(quantidadeItem1 != null && this.valorVendaUnitarioProvider1 != null) {
			firstValue = quantidadeItem1 * this.valorVendaUnitarioProvider1.doubleValue();
		}

		if(quantidadeItem2 != null && this.valorVendaUnitarioProvider2 != null) {
			secondValue = quantidadeItem2 * this.valorVendaUnitarioProvider2.doubleValue();
		}
		
		return BigDecimal.valueOf(firstValue + secondValue);
	}

	private void buildAttributsTest(Integer quantidadeItem1, Integer quantidadeItem2) {
		this.items.get(0).setQuantity(quantidadeItem1);
		this.items.get(1).setQuantity(quantidadeItem2);
		Mockito.when(simRepository.count(Example.of(Sim.builder().provider(provider1).status(SimChipStatus.FREE).build(), matcher))).thenReturn(this.quantityInStockProvider1);
		Mockito.when(simRepository.count(Example.of(Sim.builder().provider(provider2).status(SimChipStatus.FREE).build(), matcher))).thenReturn(this.quantityInStockProvider2);
	}

}
